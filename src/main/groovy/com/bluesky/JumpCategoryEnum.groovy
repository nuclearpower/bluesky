package com.bluesky

enum JumpCategoryEnum {

  BELLY_FLY("jump.category.enum.bellyFly.label", ["Athlete"] as String[]),
  FREE_FLY("jump.category.enum.freeFly.label", ["Athlete"] as String[]),
  TANDEM("jump.category.enum.tandem.label", ["Athlete", "Student", "Tandem"] as String[]),
  AFF("jump.category.enum.aff.label", ["Athlete", "Student"] as String[]),
  BBF("jump.category.enum.bbf.label", ["Athlete"] as String[]),
  ASL("jump.category.enum.asl.label", ["Athlete", "Student"] as String[]),
  TR("jump.category.enum.tr.label", ["Athlete"] as String[]),
  TRV("jump.category.enum.trv.label", ["Athlete"] as String[]),
  FUN("jump.category.enum.fun.label", ["Athlete"] as String[]);
  
  public final String i18nKey
  public final String[] availableTo

  private JumpCategoryEnum(String i18nKey, String[] availableTo) {
    this.i18nKey = i18nKey
    this.availableTo = availableTo
  }
}
