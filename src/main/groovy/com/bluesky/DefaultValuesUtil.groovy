package com.bluesky

class DefaultValuesUtil {

  static DefaultValues transferData(DefaultValues from, DefaultValues to) {
    to.takeOff = from.takeOff;
    to.tandemSkydive = from.tandemSkydive
    to.parachutePacking = from.parachutePacking
    to.equipmentRental = from.equipmentRental

    return to
  }
  
}
