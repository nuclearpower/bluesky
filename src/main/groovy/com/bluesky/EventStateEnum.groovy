package com.bluesky

enum EventStateEnum {
  OPEN("event.state.enum.open.label"),
  IN_PROGRESS("event.state.enum.inprogress.label"),
  CLOSED("event.state.enum.closed.label");
  
  public final String i18nKey

  private EventStateEnum(String i18nKey) {
    this.i18nKey = i18nKey
  }
}
