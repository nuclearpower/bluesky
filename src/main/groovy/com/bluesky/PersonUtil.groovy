package com.bluesky

class PersonUtil {

  static Person transferData(Person from, Person to) {
    to.name = from.name;
    to.phoneNumber = from.phoneNumber
    to.emergencyPhoneNumber = from.emergencyPhoneNumber
    to.bloodGroupAndRhFactor = from.bloodGroupAndRhFactor
    to.weight = from.weight
    to.medicalObservations = from.medicalObservations
    to.birthDate = from.birthDate
    to.personCategory = from.personCategory
    to.cbpqRegistration = from.cbpqRegistration
    to.skydiverCategory = from.skydiverCategory

    return to
  }
  
}
