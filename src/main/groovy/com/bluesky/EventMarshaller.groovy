package com.bluesky

import grails.converters.JSON

class EventMarshaller {

  void register() {
    JSON.registerObjectMarshaller(Event) { Event event ->
      return [ 
        id : event.id,
        name : event.name,
        startDate : event.startDate.format("yyyy-MM-dd"),
        finishDate : event.finishDate.format("yyyy-MM-dd"),
        state: event.state
      ]
    }
  }
}
