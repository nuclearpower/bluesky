package com.bluesky

class ManifestUtil {
  
  private static def printManifest(Manifest manifest) {
    println "*** MANIFEST ***"
    println "id: ${manifest.id}"
    println "creationDate: ${manifest.creationDate}"
    println "observation: ${manifest.observation}"
    println "done: ${manifest.done}"
    println "eventId: ${manifest.eventId}"
    println "altitude: ${manifest.altitude}"
    if (manifest.aircraft) {
      println "  -- Aircraft -- "
      println "    registration: ${manifest.aircraft.registration}"
      println "    model: ${manifest.aircraft.model}"
      println "    capacity: ${manifest.aircraft.capacity}"      
    } else {
      println "  -- NO AIRCRAFT -- "
    }
    if (manifest.items) {
      println "  -- Manifest Items -- "
      for (ManifestItem item in manifest.items) {
        println "    jumpCategory: ${item.jumpCategory}"
        println "    observations: ${item.observations}"
        println "    calculatedValue: ${item.calculatedValue}"
        println "    discount: ${item.discount}"
        println "    extraValue: ${item.extraValue}"
        println "    paidValue: ${item.paidValue}"
        if (item.manifestPerson) {
          println "    -- ManifestPerson --"
          println "        role: ${item.manifestPerson.role}"
          if (item.manifestPerson.person) {
            println "        -- Person --"
            println "            name: ${item.manifestPerson.person.name}"
            println "            cpf: ${item.manifestPerson.person.cpf}"
            println "            phoneNumber: ${item.manifestPerson.person.phoneNumber}"
            println "            emergencyPhoneNumber: ${item.manifestPerson.person.emergencyPhoneNumber}"
            println "            bloodGroupAndRhFactor: ${item.manifestPerson.person.bloodGroupAndRhFactor}"
            println "            weight: ${item.manifestPerson.person.weight}"
            println "            medicalObservations: ${item.manifestPerson.person.medicalObservations}"
            println "            birthDate: ${item.manifestPerson.person.birthDate}"
            println "            personCategory: ${item.manifestPerson.person.personCategory}"
            println "            cbpqRegistration: ${item.manifestPerson.person.personCategory}"
            println "            skydiverCategory: ${item.manifestPerson.person.skydiverCategory}"
            println "            studentCourseType: ${item.manifestPerson.person.studentCourseType}"
            println "            studentAffLevel: ${item.manifestPerson.person.studentAffLevel}"
          } else {
            println "        -- NO PERSON --"
          }
          if (item.manifestPerson.equipment) {
            println "        -- Equipment --"
            println "            container: ${item.manifestPerson.equipment.container}"
            println "            mainCanopySize: ${item.manifestPerson.equipment.mainCanopySize}"
            if (item.manifestPerson.equipment.owner) {
              println "            -- Owner --"
              println "                name: ${item.manifestPerson.person.name}"
              println "                cpf: ${item.manifestPerson.person.cpf}"
              println "                phoneNumber: ${item.manifestPerson.person.phoneNumber}"
              println "                emergencyPhoneNumber: ${item.manifestPerson.person.emergencyPhoneNumber}"
              println "                bloodGroupAndRhFactor: ${item.manifestPerson.person.bloodGroupAndRhFactor}"
              println "                weight: ${item.manifestPerson.person.weight}"
              println "                medicalObservations: ${item.manifestPerson.person.medicalObservations}"
              println "                birthDate: ${item.manifestPerson.person.birthDate}"
              println "                personCategory: ${item.manifestPerson.person.personCategory}"
              println "                cbpqRegistration: ${item.manifestPerson.person.personCategory}"
              println "                skydiverCategory: ${item.manifestPerson.person.skydiverCategory}"
              println "                studentCourseType: ${item.manifestPerson.person.studentCourseType}"
              println "                studentAffLevel: ${item.manifestPerson.person.studentAffLevel}"
            } else {
              println "            -- NO OWNER --"
            }
          }
        } else {
          println "    -- NO MANIFESTPERSON --"
        }
        if (item.assistants) {
          println "    -- Assistants --"
          for (ManifestPerson assistant in item.assistants) {
            println "        role: ${assistant.role}"
            if (assistant.person) {
              println "        -- Person --"
              println "            name: ${assistant.person.name}"
              println "            cpf: ${assistant.person.cpf}"
              println "            phoneNumber: ${assistant.person.phoneNumber}"
              println "            emergencyPhoneNumber: ${assistant.person.emergencyPhoneNumber}"
              println "            bloodGroupAndRhFactor: ${assistant.person.bloodGroupAndRhFactor}"
              println "            weight: ${assistant.person.weight}"
              println "            medicalObservations: ${assistant.person.medicalObservations}"
              println "            birthDate: ${assistant.person.birthDate}"
              println "            personCategory: ${assistant.person.personCategory}"
              println "            cbpqRegistration: ${assistant.person.personCategory}"
              println "            skydiverCategory: ${assistant.person.skydiverCategory}"
              println "            studentCourseType: ${assistant.person.studentCourseType}"
              println "            studentAffLevel: ${assistant.person.studentAffLevel}"
            } else {
              println "        -- NO PERSON --"
            }
            if (assistant.equipment) {
              println "        -- Equipment --"
              println "            container: ${assistant.equipment.container}"
              println "            mainCanopySize: ${assistant.equipment.mainCanopySize}"
              if (assistant.equipment.owner) {
                println "            -- Owner --"
                println "                name: ${assistant.equipment.owner.name}"
                println "                cpf: ${assistant.equipment.owner.cpf}"
                println "                phoneNumber: ${assistant.equipment.owner.phoneNumber}"
                println "                emergencyPhoneNumber: ${assistant.equipment.owner.emergencyPhoneNumber}"
                println "                bloodGroupAndRhFactor: ${assistant.equipment.owner.bloodGroupAndRhFactor}"
                println "                weight: ${assistant.equipment.owner.weight}"
                println "                medicalObservations: ${assistant.equipment.owner.medicalObservations}"
                println "                birthDate: ${assistant.equipment.owner.birthDate}"
                println "                personCategory: ${assistant.equipment.owner.personCategory}"
                println "                cbpqRegistration: ${assistant.equipment.owner.personCategory}"
                println "                skydiverCategory: ${assistant.equipment.owner.skydiverCategory}"
                println "                studentCourseType: ${assistant.equipment.owner.studentCourseType}"
                println "                studentAffLevel: ${assistant.equipment.owner.studentAffLevel}"
              } else {
                println "            -- NO OWNER --"
              }
            } else {
              println "        -- NO EQUIPMENT --"
            }
          }
        } else {
          println "    -- NO ASSISTANTS --"
        }
      }
    } else {
      println "  -- NO MANIFEST ITEMS -- "
    }
  }

}
