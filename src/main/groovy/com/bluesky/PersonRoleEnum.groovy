package com.bluesky

enum PersonRoleEnum {
  CAMERAMAN("person.role.enum.cameraman.label"),
  INSTRUCTOR("person.role.enum.instructor.label"),
  TANDEM_PILOT("person.role.enum.tandemPilot.label"),
  TANDEM_PASSENGER("person.role.enum.tandemPassenger.label"),
  COACH("person.role.enum.coach.label"),
  SKYDIVER("person.role.enum.skydiver.label");
  
  public final String i18nKey

  private PersonRoleEnum(String i18nKey) {
    this.i18nKey = i18nKey
  }
}
