package com.bluesky

import grails.testing.mixin.integration.Integration
import grails.transaction.*
import spock.lang.Specification
import com.bluesky.Aircraft

@Integration
@Rollback
class AircraftSpec extends Specification {

    Integer aircraftCountBeforeTests
    final String aircraftRegistration = "PT-TEST"
    final String aircraftModel = "TEST MODEL"
    final Integer aircraftCapacity = 4

    final String aircraftModelEdit = "MODEL EDIT"
    final Integer aircraftCapacityEdit = 5

    def setup() {
      aircraftCountBeforeTests = Aircraft.count()
    }

    def cleanup() {
    }

    void setupData() {
      //These operations are rollback on the end of test execution
      new Aircraft(registration: aircraftRegistration,
                    model: aircraftModel,
                    capacity: aircraftCapacity).save(flush: true)
    }

    void "test aircraft insertion in database"() {
        given:
          setupData()
        expect:
          Aircraft.count() == (aircraftCountBeforeTests + 1)
    }

    void "test aircraft remove from database"() {
        given:
          setupData()
          Aircraft air = Aircraft.findByRegistration(aircraftRegistration)
          air.delete(flush: true)
          air = Aircraft.findByRegistration(aircraftRegistration)
        expect:
          !air
    }

    void "test searching by an existing aircraft registration"() {
        given:
          setupData()
          Aircraft air = Aircraft.findByRegistration(aircraftRegistration)
        expect:
          air.registration == aircraftRegistration
    }

    void "test searching by a non existing aircraft registration"() {
        given:
          setupData()
          Aircraft air = Aircraft.findByRegistration("Non Existing Registration")
        expect:
          !air
    }

    void "test if all data were saved in database"() {
      given:
        setupData()
        Aircraft air = Aircraft.findByRegistration(aircraftRegistration)
      expect:
        air.registration == aircraftRegistration
        air.model == aircraftModel
        air.capacity == aircraftCapacity
    }

    void "test if aircraft was edited and saved in database"() {
      given:
        setupData()
        Aircraft air = Aircraft.findByRegistration(aircraftRegistration)
        air.model = aircraftModelEdit
        air.capacity = aircraftCapacityEdit
        air.save(flush:true)
        air = null
        air = Aircraft.findByRegistration(aircraftRegistration)
      expect:
        air.registration == aircraftRegistration
        air.model == aircraftModelEdit
        air.capacity == aircraftCapacityEdit
    }
}
