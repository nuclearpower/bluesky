package com.bluesky

import grails.testing.mixin.integration.Integration
import grails.transaction.*
import spock.lang.Specification
import com.bluesky.DefaultValues

@Integration
@Rollback
class DefaultValuesSpec extends Specification {

    Integer defaultValuesCountBeforeTests
    final Integer defaultValuesClientId = 2
    final Double defaultValuesTakeOff = 150.89
    final Double defaultValuesTandem = 680
    final Double defaultValuesParachutePacking = 15.5
    final Double defaultValuesEquipmentRental = 50

    final Double defaultValuesTakeOffEdit = 300.01
    final Double defaultValuesTandemEdit = 340.52
    final Double defaultValuesParachutePackingEdit = 31.02
    final Double defaultValuesEquipmentRentalEdit = 55

    def setup() {
      defaultValuesCountBeforeTests = DefaultValues.count()
    }

    def cleanup() {
    }

    void setupData() {
      //These operations are rollback on the end of test execution
      new DefaultValues(clientId: defaultValuesClientId,
                    takeOff: defaultValuesTakeOff,
                    tandemSkydive: defaultValuesTandem,
                    parachutePacking: defaultValuesParachutePacking,
                    equipmentRental: defaultValuesEquipmentRental).save(flush: true)
    }

    void "test defaultValues insertion in database"() {
        given:
          setupData()
        expect:
          DefaultValues.count() == (defaultValuesCountBeforeTests + 1)
    }

    void "test defaultValues remove from database"() {
        given:
          setupData()
          DefaultValues defValues = DefaultValues.findByClientId(defaultValuesClientId)
          defValues.delete(flush: true)
          defValues = DefaultValues.findByClientId(defaultValuesClientId)
        expect:
          !defValues
    }

    void "test searching by an existing defaultValues client id"() {
        given:
          setupData()
          DefaultValues defValues = DefaultValues.findByClientId(defaultValuesClientId)
        expect:
          defValues
    }

    void "test searching by a non existing defaultValues client id"() {
        given:
          setupData()
          DefaultValues defValues = DefaultValues.findByClientId(999999)
        expect:
          !defValues
    }

    void "test if all data were saved in database"() {
      given:
        setupData()
        DefaultValues defValues = DefaultValues.findByClientId(defaultValuesClientId)
      expect:
        defValues.clientId == defaultValuesClientId
        defValues.takeOff == defaultValuesTakeOff
        defValues.tandemSkydive == defaultValuesTandem
        defValues.parachutePacking == defaultValuesParachutePacking
        defValues.equipmentRental == defaultValuesEquipmentRental
    }

    void "test edit default values"() {
      given:
        setupData()
        DefaultValues defValues = DefaultValues.findByClientId(defaultValuesClientId)
        defValues.takeOff = defaultValuesTakeOffEdit
        defValues.tandemSkydive = defaultValuesTandemEdit
        defValues.parachutePacking = defaultValuesParachutePackingEdit
        defValues.equipmentRental = defaultValuesEquipmentRentalEdit
        defValues.save(flush:true)
        DefaultValues defValuesEdit = DefaultValues.findByClientId(defaultValuesClientId)
      expect:
        defValuesEdit.clientId == defaultValuesClientId
        defValuesEdit.takeOff == defaultValuesTakeOffEdit
        defValuesEdit.tandemSkydive == defaultValuesTandemEdit
        defValuesEdit.parachutePacking == defaultValuesParachutePackingEdit
        defValuesEdit.equipmentRental == defaultValuesEquipmentRentalEdit
    }
}
