package bluesky

import grails.testing.mixin.integration.Integration
import grails.transaction.*
import spock.lang.Specification
import com.bluesky.Location

@Integration
@Rollback
class LocationSpec extends Specification {

    Integer locationCountBeforeTests
    final String locationName = "Test Location"

    def setup() {
      locationCountBeforeTests = Location.count()
    }

    def cleanup() {
    }

    void setupData() {
      //These operations are rollback on the end of test execution
      new Location(name: locationName).save(flush: true)
    }

    void "test location insertion in database"() {
        given:
          setupData()
        expect:
          Location.count() == (locationCountBeforeTests + 1)
    }

    void "test location remove from database"() {
        given:
          setupData()
          Location loc = Location.findByName(locationName)
          loc.delete(flush: true)
          loc = Location.findByName(locationName)
        expect:
          !loc
    }

    void "test searching by an existing location name"() {
        given:
          setupData()
          Location loc = Location.findByName(locationName)
        expect:
          loc.getName().equals(locationName)
    }

    void "test searching by a non existing location name"() {
        given:
          setupData()
          Location loc = Location.findByName("Non Existing Location")
        expect:
          !loc
    }
}
