package com.bluesky

import grails.testing.mixin.integration.Integration
import grails.transaction.*
import spock.lang.Specification
import com.bluesky.Person

@Integration
@Rollback
class PersonSpec extends Specification {

    Integer personCountBeforeTests
    final String personCpf = "999.888.777-66"
    final String personName = "Person Test"
    final String personPhoneNumber = "+55 51 99876 5432"
    final String personEmergencyPhoneNumber = "+55 51 3333 4444"
    final String personBloodGroupAndRhFactor = "AB -"
    final Double personWeight = 102.57
    final String personMedicalObservations = "Aliquam eu sapien imperdiet, aliquet velit vitae, feugiat lectus. Etiam lacinia risus a erat ultrices euismod. Fusce at congue odio."
    final Date personBirthDate = Date.parse("yyyy-MM-dd", "1990-01-02")
    final String personCategory = "Athlete"
    final Integer personCbpqRegistration = 12345
    final String personSkydiverCategory = "AI"
    final String personStudentCourseType = "AFF"
    final Integer personStudentAffLevel = 4

    final String personNameEdit = "Person Name Edit"
    final String personPhoneNumberEdit = "+55 51 9999 9999"
    final String personEmergencyPhoneNumberEdit = "+55 51 4444 5555"
    final String personBloodGroupAndRhFactorEdit = "O -"
    final Double personWeightEdit = 82.45
    final String personMedicalObservationsEdit = "Bees alergy"
    final Date personBirthDateEdit = Date.parse("yyyy-MM-dd", "1995-01-02")
    final String personCategoryEdit = "Weekend Athlete"
    final Integer personCbpqRegistrationEdit = 23456
    final String personSkydiverCategoryEdit = "AIII"
    final String personStudentCourseTypeEdit = "AGG"
    final Integer personStudentAffLevelEdit = 5

    def setup() {
      personCountBeforeTests = Person.count()
    }

    def cleanup() {
    }

    void setupData() {
      //These operations are rollback on the end of test execution
      new Person(name: personName,
                cpf: personCpf,
                phoneNumber: personPhoneNumber,
                emergencyPhoneNumber: personEmergencyPhoneNumber,
                bloodGroupAndRhFactor: personBloodGroupAndRhFactor,
                weight: personWeight,
                medicalObservations: personMedicalObservations,
                birthDate: personBirthDate,
                personCategory: personCategory,
                cbpqRegistration: personCbpqRegistration,
                skydiverCategory: personSkydiverCategory,
                studentCourseType: personStudentCourseType,
                studentAffLevel: personStudentAffLevel).save(flush: true)
    }

    void "test person insertion in database"() {
        given:
          setupData()
        expect:
          Person.count() == (personCountBeforeTests + 1)
    }

    void "test person remove from database"() {
        given:
          setupData()
          Person person = Person.findByCpf(personCpf)
          person.delete(flush: true)
          person = Person.findByCpf(personCpf)
        expect:
          !person
    }

    void "test searching by an existing person cpf"() {
        given:
          setupData()
          Person person = Person.findByCpf(personCpf)
        expect:
          person
    }

    void "test searching by a non existing person cpf"() {
        given:
          setupData()
          Person person = Person.findByCpf(111)
        expect:
          !person
    }

    void "test if all data were saved in database"() {
      given:
        setupData()
        Person person = Person.findByCpf(personCpf)
      expect:
        person.name == personName
        person.cpf == personCpf
        person.phoneNumber == personPhoneNumber
        person.emergencyPhoneNumber == personEmergencyPhoneNumber
        person.bloodGroupAndRhFactor == personBloodGroupAndRhFactor
        person.weight == personWeight
        person.medicalObservations == personMedicalObservations
        person.birthDate == personBirthDate
        person.personCategory == personCategory
        person.cbpqRegistration == personCbpqRegistration
        person.skydiverCategory == personSkydiverCategory
        person.studentCourseType == personStudentCourseType
        person.studentAffLevel == personStudentAffLevel
    }

    void "test edit person data"() {
      given:
        setupData()
        Person person = Person.findByCpf(personCpf)
        person.name = personNameEdit
        person.phoneNumber = personPhoneNumberEdit
        person.emergencyPhoneNumber = personEmergencyPhoneNumberEdit
        person.bloodGroupAndRhFactor = personBloodGroupAndRhFactorEdit
        person.weight = personWeightEdit
        person.medicalObservations = personMedicalObservationsEdit
        person.birthDate = personBirthDateEdit
        person.personCategory = personCategoryEdit
        person.cbpqRegistration = personCbpqRegistrationEdit
        person.skydiverCategory = personSkydiverCategoryEdit
        person.studentCourseType = personStudentCourseTypeEdit
        person.studentAffLevel = personStudentAffLevelEdit
        person.save(flush:true)
        Person personEdit = Person.findByCpf(personCpf)
      expect:
        personEdit.name == personNameEdit
        personEdit.phoneNumber == personPhoneNumberEdit
        personEdit.emergencyPhoneNumber == personEmergencyPhoneNumberEdit
        personEdit.bloodGroupAndRhFactor == personBloodGroupAndRhFactorEdit
        personEdit.weight == personWeightEdit
        personEdit.medicalObservations == personMedicalObservationsEdit
        personEdit.birthDate == personBirthDateEdit
        personEdit.personCategory == personCategoryEdit
        personEdit.cbpqRegistration == personCbpqRegistrationEdit
        personEdit.skydiverCategory == personSkydiverCategoryEdit
        personEdit.studentCourseType == personStudentCourseTypeEdit
        personEdit.studentAffLevel == personStudentAffLevelEdit
    }

}
