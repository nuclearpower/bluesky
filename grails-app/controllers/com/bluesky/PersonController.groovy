package com.bluesky

import com.bluesky.PersonUtil

class PersonController {

  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  def index() { }

  def list() {
    def personList = Person.list()
    render personList.encodeAsJSON()
  }

  def remove() {
    String cpf = params['cpf']
    Person personToDelete = Person.findByCpf(cpf)
    personToDelete.delete(flush: true)
    render "true"
  }

  def save() {
    def slurper = new groovy.json.JsonSlurper()
    def result = slurper.parseText(params['person'])    
    Person person = result as Person
    Person personPersisted = Person.findByCpf(person.cpf);
    if (personPersisted != null) {
      personPersisted = PersonUtil.transferData(person, personPersisted)
    } else {
      personPersisted = person
    }
    if (!personPersisted.save(failOnError: true, flush: true)) {
      render "UpdateFailed"
    }
    render "Updated"
  }

  def insert() {
    def slurper = new groovy.json.JsonSlurper()
    def result = slurper.parseText(params['person'])    
    Person person = result as Person
    Person personPersisted = Person.findByCpf(person.cpf);
    if (personPersisted != null) {
      render "InsertDuplicated"
    } 
    if (!person.save(failOnError: true, flush: true)) {
      render "InsertFailed"
    }
    render "Inserted"
  }
}
