package com.bluesky

class EventController {

  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  def index() { 
  }

  def list() {

    def list = Event.list()
    render list.encodeAsJSON()
  }

  def insert() {

  }

  def edit() {

  }

  def remove() {

  }

  def setCurrent() {

    def eventId = params.int('eventId')

    Event.list().each {

      boolean changed = false
      if(it.id == eventId) {
        it.state = EventStateEnum.IN_PROGRESS
        changed = true
      } else if(it.state == EventStateEnum.IN_PROGRESS) {
        it.state = EventStateEnum.OPEN
        changed = true
      }
      if(changed) {
        it.save(failOnError:true, flush:true)
      }
    }
  }
}
