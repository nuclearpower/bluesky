package com.bluesky

import com.bluesky.Manifest
import org.springframework.context.MessageSource

class ManifestController {

  MessageSource messageSource

  def index() { 
  }

  def list() {
    Integer eventId = params.int('currentEventId')
    Boolean today = params.boolean('today') ?: false    
    def creationDateBegin = new Date().parse("yyyy/MM/dd HH:mm:ss", new Date().format("yyyy/MM/dd") + " 00:00:00")
    def creationDateEnd = new Date().parse("yyyy/MM/dd HH:mm:ss", new Date().format("yyyy/MM/dd") + " 23:59:59")
    def criteria = Manifest.createCriteria()
    def manifests = criteria.list {
      and {
        if (today) {
          between("creationDate", creationDateBegin, creationDateEnd)
        }
        eq("eventId", eventId)
      }
      order("id", "desc")
    }

    def manifestsJson = 
      manifests.collect {
        manifestToJson(it)
      }

    render manifestsJson.encodeAsJSON()
  }

  def remove() {
    String id = params['id']
    Manifest manifestToDelete = Manifest.findById(id)
    manifestToDelete.delete(flush: true)
    render "true"
  }

  def insert() {
    
    Manifest manifest = createManifestFromParams(params)
    def result = ""
    if (!manifest.save(failOnError: true, flush:true)) {
      result = "InsertFailed"
    } else {
      result = manifestToJson(manifest).encodeAsJSON()
    }
    render result
  }

  def edit() {
    Manifest manifest = createManifestFromParams(params)
    def result = manifestToJson(manifest).encodeAsJSON()
    if (!manifest.save(failOnError: true, flush:true)) {
      result = "EditFailed"
    } 
    render result
  }

  private Manifest createManifestFromParams(def params) {
    def jsonSlurper = new groovy.json.JsonSlurper()
    def jsonObject = jsonSlurper.parseText(params['manifest'])

    Aircraft aircraft = Aircraft.findByRegistration(jsonObject.aircraft.registration)
    Integer manifestId = jsonObject.id
    Integer eventId = jsonObject.eventId
    Integer altitude = jsonObject.altitude as Integer
    def lstItems = jsonObject.items
    List<ManifestItem> manifestItems = []
    for (def item in lstItems) {
      Person person = Person.findByCpf(item.manifestPerson.person.cpf)
      Equipment equipment = null
      // There is no equipment for a Tandem passanger
      if (item.manifestPerson.equipment.container) {
         Person equipmentLocator = null
         if (item.manifestPerson.equipment.owner) {
            equipmentLocator = Person.findByCpf(item.manifestPerson.equipment.owner.cpf)
         }
         equipment = new Equipment(
            container: item.manifestPerson.equipment.container,
            mainCanopySize: item.manifestPerson.equipment.mainCanopySize,
            owner: equipmentLocator
          )
      }
      PersonRoleEnum role = item.manifestPerson.role.key as PersonRoleEnum
      ManifestPerson manifestPerson = new ManifestPerson(
          person: person,
          equipment: equipment,
          role: role
        )
      JumpCategoryEnum jumpCategory = item.jumpCategory.key as JumpCategoryEnum
      def lstAssistants = item.assistants
      List<ManifestPerson> assistants = []
      for (def assistant in lstAssistants) {
        if (assistant.person) {
          Person personAssistant = Person.findByCpf(assistant.person.cpf)
          if (personAssistant) {
            Person assistantEquipmentLocator = null
            if (assistant.equipment.owner) {
              assistantEquipmentLocator = Person.findByCpf(assistant.equipment.owner.cpf)
            }
            Equipment equipmentAssistant = new Equipment(
                container: assistant.equipment.container,
                mainCanopySize: assistant.equipment.mainCanopySize,
                owner: assistantEquipmentLocator
              )
            PersonRoleEnum assistantRole = assistant.role.key as PersonRoleEnum
            ManifestPerson assistantManifestPerson = new ManifestPerson(
              person: personAssistant,
              equipment: equipmentAssistant,
              role: assistantRole
            )
            assistants << assistantManifestPerson
          }
        }
      }
      ManifestItem manifestItem = new ManifestItem(
          manifestPerson: manifestPerson,
          jumpCategory: jumpCategory,
          assistants: assistants
        )
      manifestItems << manifestItem
    }
    Manifest manifest = new Manifest(
        id: manifestId,
        aircraft: aircraft,
        items: manifestItems,
        eventId: eventId,
        altitude: altitude
      )

    if (manifestId) {
      Manifest manifestPersisted = Manifest.findById(manifestId)
      loadManifestData(manifest, manifestPersisted)
      manifest = manifestPersisted
    }

    return manifest
  }

  private def loadManifestData(Manifest fromManifest, Manifest toManifest) {
    toManifest.aircraft = fromManifest.aircraft
    toManifest.observation = fromManifest.observation
    toManifest.done = fromManifest.done
    toManifest.eventId = fromManifest.eventId
    toManifest.altitude = fromManifest.altitude

    toManifest.items = fromManifest.items
    return toManifest
  }

  def setManifestDone() {
    Integer id = params.int('id')
    Boolean done = params.boolean('done')
    Manifest manifestToChange = Manifest.findById(id)
    manifestToChange.done = done
    manifestToChange.save(failOnError:true, flush: true)
    render "true"
  }

  private def manifestToJson(Manifest manifest) {
    [
      id: manifest.id,
      creationDate: manifest.creationDate.format('dd/MM/yyyy'),
      eventId: manifest.eventId,
      done: manifest.done,
      aircraft: [
        registration: manifest.aircraft.registration,
        model: manifest.aircraft.model,
        capacity: manifest.aircraft.capacity
      ],
      items: manifest.items.collect {
        manifestItemToJson(it)            
      },
      observation: manifest.observation,
      altitude: manifest.altitude
    ]
  }

  private def manifestItemToJson(ManifestItem manifestItem) {
    [
      manifestPerson: manifestPersonToJson(manifestItem.manifestPerson),
      jumpCategory: [
          key: manifestItem.jumpCategory.name(),
          value: messageSource.getMessage(manifestItem.jumpCategory.i18nKey, null, null, new Locale('pt','BR')),
          ],
      observations: manifestItem.observations,
      assistants: manifestItem.assistants.collect {
        manifestPerson: assistantManifestPersonToJson(it)
      }
    ]
  }

  private def manifestPersonToJson(ManifestPerson manifestPerson) {
    [
      person: [
        name: manifestPerson.person.name,
        personCategory: manifestPerson.person.personCategory,
        skydiverCategory: manifestPerson.person.skydiverCategory,
        studentCourseType: manifestPerson.person.studentCourseType,
        studentAffLevel: manifestPerson.person.studentAffLevel,
        weight: manifestPerson.person.weight,
        cpf: manifestPerson.person.cpf
      ],
      equipment: [
          container: manifestPerson.equipment?.container,
          mainCanopySize: manifestPerson.equipment?.mainCanopySize,
          owner: equipmentOwnerToJson(manifestPerson.equipment?.owner)
      ],
      role: [
        key: manifestPerson.role.name(),
        value: messageSource.getMessage(manifestPerson.role.i18nKey, null, null, new Locale('pt','BR')),
      ]
    ]
  }

  private def assistantManifestPersonToJson(ManifestPerson manifestPerson) {
    [
      person: [
        name: manifestPerson?.person?.name,
        cpf: manifestPerson?.person?.cpf
      ],
      equipment: assistantEquipmentToJson(manifestPerson.equipment),
      role: [
        key: manifestPerson.role.name(),
        value: messageSource.getMessage(manifestPerson.role.i18nKey, null, null, new Locale('pt','BR'))
      ]
    ]
  }

  private def assistantEquipmentToJson(Equipment equipment) {
    [
      container: equipment.container,
      mainCanopySize: equipment.mainCanopySize,
      owner: equipmentOwnerToJson(equipment.owner)
    ]
  }

  private def equipmentOwnerToJson(Person owner) {
    [
      name: owner?.name,
      cpf: owner?.cpf
    ]
  }

  def getJumpCategories() {
    def result = []
    for (JumpCategoryEnum jumpCategory in JumpCategoryEnum.values()) {
      result << [key: jumpCategory.name(),
        value: messageSource.getMessage(jumpCategory.i18nKey, null, null, new Locale('pt','BR')),
        availableTo: jumpCategory.availableTo]
    }
    result.sort{ it.value.toLowerCase() }
    render result.encodeAsJSON()
  }

}
