package com.bluesky

import com.bluesky.DefaultValues
import com.bluesky.DefaultValuesUtil

class DefaultValuesController {

  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  def index() { 
  }

  def getDefaultValues() {
    def defaultValues = DefaultValues.findById(1)
    defaultValues = (defaultValues ? defaultValues : createEmptyDefaultValues())
    render defaultValues.encodeAsJSON()
  }

  private DefaultValues createEmptyDefaultValues() {
    new DefaultValues(clientId: 1, 
        takeOff: 0, 
        tandemSkydive: 0,
        parachutePacking: 0,
        equipmentRental: 0)
  }

  def save() {
    def slurper = new groovy.json.JsonSlurper()
    def result = slurper.parseText(params['defaultValues'])    
    DefaultValues defaultValues = result as DefaultValues  
    DefaultValues defaultValuesPersisted = DefaultValues.findById(defaultValues.clientId)

    if (defaultValuesPersisted) {
      defaultValuesPersisted = DefaultValuesUtil.transferData(defaultValues, defaultValuesPersisted)
    } else {      
      defaultValuesPersisted = defaultValues
    }
    if (!defaultValuesPersisted.save(failOnError: true, flush: true)) {
      render "UpdateFailed"
    }
    render "Updated"
  }

}
