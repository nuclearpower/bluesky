package com.bluesky

class AircraftController {

  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  def index() { 

    respond Aircraft.list()
  }

  def list() {

    def aircraftList = Aircraft.list()
    render aircraftList.encodeAsJSON()
  }

  def insert() {
    def jsonSlurper = new groovy.json.JsonSlurper()
    Aircraft aircraft = jsonSlurper.parseText(params['aircraft']) as Aircraft
    Aircraft aircraftPersisted = Aircraft.findByRegistration(aircraft.registration)

    def result = ""
    if(aircraftPersisted != null) {
      result = "InsertDuplicated"
    } else if(!aircraft.save(failOnError: true, flush:true)) {
      result = "InsertFailed"
    } else {
      result = "Inserted"
    }
    render result
  }

  def edit() {
    def jsonSlurper = new groovy.json.JsonSlurper()
    Aircraft aircraft = jsonSlurper.parseText(params['aircraft']) as Aircraft
    Aircraft aircraftPersisted = Aircraft.findByRegistration(aircraft.registration)

    aircraftPersisted.model = aircraft.model
    aircraftPersisted.capacity = aircraft.capacity

    def result ="Edited"
    if(!aircraftPersisted.save(failOnError: true, flush:true)) {
      result = "EditFailed"
    }
    render result
  }

  def remove() {

    String registration = params['registration']
    Aircraft aircraftToDelete = Aircraft.findByRegistration(registration)
    aircraftToDelete.delete(flush: true)
    render "true"
  }
}
