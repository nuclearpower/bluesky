package com.bluesky

class LocationController {

  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  def index() { 
  }

  def list() {

    def list = Location.list()
    render list.encodeAsJSON()
  }

  def insert() {
    def jsonSlurper = new groovy.json.JsonSlurper()
    Location location = jsonSlurper.parseText(params['location']) as Location
    Location locationPersisted = Location.findById(location.id)

    def result = ""
    if(locationPersisted != null) {
      result = "InsertDuplicated"
    } else if(!location.save(failOnError: true, flush:true)) {
      result = "InsertFailed"
    } else {
      result = location.encodeAsJSON()
    }
    render result
  }

  def edit() {
    def jsonSlurper = new groovy.json.JsonSlurper()
    def location = jsonSlurper.parseText(params['location'])
    Location locationPersisted = Location.findById(location.id)

    locationPersisted.name = location.name

    def result ="Edited"
    if(!locationPersisted.save(failOnError: true, flush:true)) {
      result = "EditFailed"
    }
    render result
  }

  def remove() {

    Integer id = params.int('id')
    Location locationToDelete = Location.findById(id)
    locationToDelete.delete(flush: true)
    render "true"
  }
}
