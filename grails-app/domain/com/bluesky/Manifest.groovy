package com.bluesky

class Manifest {

  Date creationDate = new Date()
  Aircraft aircraft
  List<ManifestItem> items
  String observation
  Boolean done = false
  Integer eventId
  Integer altitude

  static mapping = {
    version false
  }

  static constraints = {
    observation blank: true, nullable: true
  }
}
