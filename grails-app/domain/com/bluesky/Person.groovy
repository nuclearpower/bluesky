package com.bluesky

class Person {

  String name
  String cpf
  String phoneNumber
  String emergencyPhoneNumber
  String bloodGroupAndRhFactor
  Double weight
  String medicalObservations
  Date birthDate
  String personCategory

  Integer cbpqRegistration
  String skydiverCategory 

  String studentCourseType
  Integer studentAffLevel

  
  static mapping = {
    version false
    table 'person'
    id name: 'cpf', generator: 'assigned', column: 'pers_cpf', sqlType: 'varchar2', length: 14
    name column: 'pers_name', sqlType: 'varchar2', length: 100
    phoneNumber column: 'pers_phone', sqlType: 'varchar2', length: 30
    emergencyPhoneNumber column: 'pers_phone_emergency', sqlType: 'varchar2', length: 30
    bloodGroupAndRhFactor column: 'pers_blood_group_rh_factor', sqlType: 'varchar2', length: 5
    weight column: 'pers_weight', sqlType: 'double', length: 6, precision: 2
    medicalObservations column: 'pers_medical_obs', sqlType: 'text'
    birthDate column: 'pers_birthdate', sqlType: 'date'
    personCategory column: 'pers_category', sqlType: 'varchar2', length: 20
    cbpqRegistration column: 'pers_cbpq_registration', sqlType: 'integer'
    skydiverCategory column: 'pers_skydiver_category', sqlType: 'varchar2', length: 2
    studentCourseType column: 'pers_student_course_type', sqlType: 'varchar2', length: 10
    studentAffLevel column: 'pers_aff_level', sqlType: 'integer'
  }

  static constraints = {
      cbpqRegistration blank: true, nullable: true
      skydiverCategory blank: true, nullable: true
      birthDate blank: true, nullable: true
      phoneNumber blank: true, nullable: true
      emergencyPhoneNumber blank: true, nullable: true
      bloodGroupAndRhFactor blank: true, nullable: true
      weight blank: true, nullable: true
      medicalObservations blank: true, nullable: true
      studentCourseType blank: true, nullable: true
      studentAffLevel blank: true, nullable: true
  }
}
