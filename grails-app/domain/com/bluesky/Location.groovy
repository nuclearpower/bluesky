package com.bluesky

class Location {
  
  Integer id
  String name

  static mapping = {
    version false
    table "location"
    id column: "loca_id", sqlType: "integer"
    name column: "loca_name", sqlType: "varchar2", length: 80    
  }
}
