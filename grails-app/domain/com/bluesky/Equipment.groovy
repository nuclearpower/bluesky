package com.bluesky

class Equipment {

  String container
  Integer mainCanopySize
  Person owner

  static mapping = {
    version false
  }

  static constraints = {
    owner blank: true, nullable: true
  }
}
