package com.bluesky

class Event {

  Integer id
  String name
  Date startDate
  Date finishDate
  EventStateEnum state

  static mapping = {
    version false
    state(type: EventStateEnum)
  }
}
