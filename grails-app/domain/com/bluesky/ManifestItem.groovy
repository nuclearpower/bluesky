package com.bluesky

class ManifestItem {

  ManifestPerson manifestPerson
  JumpCategoryEnum jumpCategory //BBF (Basic Body Flight), Fun, TR (Trabalho relativo), 
                      //Tandem (Duplo), ASL (Accelerated Static Line), 
                      //AFF (Accelerated Free Fall), TRV (Trabalho relativo de velame),
                      //Free Fly
  List<ManifestPerson> assistants

  String observations

  //Extract to a new class BillReport
  Double calculatedValue
  Double discount
  Double extraValue
  Double paidValue  

  static mapping = {
    version false
  }

  static constraints = {
    observations blank: true, nullable: true
    calculatedValue blank: true, nullable: true
    discount blank: true, nullable: true
    extraValue blank: true, nullable: true
    paidValue blank: true, nullable: true
  }
}
