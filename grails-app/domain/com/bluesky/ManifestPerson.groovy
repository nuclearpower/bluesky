package com.bluesky

class ManifestPerson {

  Person person
  Equipment equipment
  PersonRoleEnum role

  static mapping = {
    version false
  }

  static constraints = {
    equipment blank: true, nullable: true
  }
}
