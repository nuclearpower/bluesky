package com.bluesky

class Aircraft {

  String registration
  String model
  Integer capacity

  static mapping = {
    version false
    table "aircraft"
    id name: 'registration', generator: 'assigned', column: 'airc_registration', sqlType: "varchar2", length: 10
    model column: 'airc_model', sqlType: "varchar2", length: 20
    capacity column: 'airc_capacity', sqlType: "smallint"
  }

  static constraints = {
    id: 'registration'
  }
}
