package com.bluesky

class DefaultValues {

  //This variable is representing the client ID
  //In the future each client will have an Id and will share the same database
  Long clientId = 1;

  Double takeOff
  Double tandemSkydive
  Double parachutePacking
  Double equipmentRental

  static mapping = {
    version false
    table 'defaultvalues'
    id name: 'clientId', generator: 'assigned', column: 'deva_client_id', sqlType: 'Long'
    takeOff column: 'deva_take_off', sqlType:'double', length:10, precision: 2
    tandemSkydive column: 'deva_tandem', sqlType:'double', length:10, precision: 2
    parachutePacking column: 'deva_parachute_packing', sqlType:'double', length:10, precision: 2
    equipmentRental column: 'deva_equipment_rental', sqlType:'double', length:10, precision: 2
  }

  static constraints = {
    takeOff blank: true, nullable: true
    tandemSkydive blank: true, nullable: true
    parachutePacking blank: true, nullable: true
    equipmentRental blank: true, nullable: true
  }

}
