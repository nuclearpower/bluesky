package bluesky

import com.bluesky.Aircraft
import com.bluesky.Location
import com.bluesky.Person
import com.bluesky.Event
import com.bluesky.EventMarshaller
import com.bluesky.EventStateEnum
import com.bluesky.Manifest
import com.bluesky.ManifestItem
import com.bluesky.ManifestPerson
import com.bluesky.JumpCategoryEnum
import com.bluesky.PersonRoleEnum
import com.bluesky.Equipment
import com.bluesky.DefaultValues

class BootStrap {

    def init = { servletContext ->
      def defaultValues = new DefaultValues(
          takeOff: 150,
          tandemSkydive: 680,
          parachutePacking: 15,
          equipmentRental: 50).save(failOnError: true)

      [ new EventMarshaller() ].each { it.register() }

      def aircraftCessna = new Aircraft(registration: 'PT-BMZ', model: 'CESSNA 180', capacity: 4)
      aircraftCessna.save(failOnError: true)
      def aircraft = new Aircraft(registration: 'PT-HUE', model: 'Boeing 787', capacity: 500)
      aircraft.save(failOnError: true)

      def personAtleta01 = new Person(name: 'Atleta 01', 
                          cpf: '99999999999', 
                          phoneNumber: '51999999999',
                          emergencyPhoneNumber: '5499999999',
                          bloodGroupAndRhFactor: 'O+',
                          weight: 74,
                          medicalObservations: 'Alérgico a plasil e pressão alta',
                          birthDate: Date.parse("yyyy-MM-dd", "1981-08-15"),
                          cbpqRegistration: 84657,
                          skydiverCategory: 'B',
                          personCategory: "Atleta");
      personAtleta01.save(failOnError: true)
      def personAlunoAff1 = new Person(name: 'Aluno AFF 1', 
                          cpf: '88888888888', 
                          phoneNumber: '51888888888',
                          emergencyPhoneNumber: '5488888888',
                          bloodGroupAndRhFactor: 'O-',
                          weight: 80,
                          medicalObservations: 'Problema de tireóide.',
                          birthDate: Date.parse("yyyy-MM-dd", "1985-02-11"),
                          cbpqRegistration: 88888,
                          skydiverCategory: 'AI',
                          personCategory: "Aluno",
                          studentCourseType: "AFF",
                          studentAffLevel: 1);
      personAlunoAff1.save(failOnError: true)
      def personPassageiroTandem = new Person(name: 'Passageiro Tandem', 
                          cpf: '88888888885', 
                          phoneNumber: '51888888888',
                          emergencyPhoneNumber: '5488888888',
                          bloodGroupAndRhFactor: 'O-',
                          weight: 80,
                          medicalObservations: 'Problema de tireóide.',
                          birthDate: Date.parse("yyyy-MM-dd", "1985-02-11"),
                          personCategory: "Passageiro Tandem");
      personPassageiroTandem.save(failOnError: true)
      def personAlunoAff4 = new Person(name: 'Aluno AFF 4', 
                          cpf: '88888888884', 
                          phoneNumber: '51888888888',
                          emergencyPhoneNumber: '5488888888',
                          bloodGroupAndRhFactor: 'O-',
                          weight: 80,
                          medicalObservations: 'Problema de tireóide.',
                          birthDate: Date.parse("yyyy-MM-dd", "1985-02-11"),
                          cbpqRegistration: 88888,
                          skydiverCategory: 'AI',
                          personCategory: "Aluno",
                          studentCourseType: "AFF",
                          studentAffLevel: 4);
      personAlunoAff4.save(failOnError: true)
      def personAtleta02 = new Person(name: 'Atleta 02', 
                          cpf: '88888888883', 
                          phoneNumber: '51888888888',
                          emergencyPhoneNumber: '5488888888',
                          bloodGroupAndRhFactor: 'O-',
                          weight: 80,
                          medicalObservations: 'Problema de tireóide.',
                          birthDate: Date.parse("yyyy-MM-dd", "1985-02-11"),
                          cbpqRegistration: 88888,
                          skydiverCategory: 'D',
                          personCategory: "Atleta");
      personAtleta02.save(failOnError: true)
      def personAtleta03 = new Person(name: 'Atleta 03', 
                          cpf: '88888888882', 
                          phoneNumber: '51888888888',
                          emergencyPhoneNumber: '5488888888',
                          bloodGroupAndRhFactor: 'O-',
                          weight: 80,
                          medicalObservations: 'Problema de tireóide.',
                          birthDate: Date.parse("yyyy-MM-dd", "1985-02-11"),
                          cbpqRegistration: 88888,
                          skydiverCategory: 'C',
                          personCategory: "Atleta");
      personAtleta03.save(failOnError: true)
      def personAtleta04 = new Person(name: 'Atleta 04', 
                          cpf: '88888888881', 
                          phoneNumber: '51888888888',
                          emergencyPhoneNumber: '5488888888',
                          bloodGroupAndRhFactor: 'O-',
                          weight: 80,
                          medicalObservations: 'Problema de tireóide.',
                          birthDate: Date.parse("yyyy-MM-dd", "1985-02-11"),
                          cbpqRegistration: 88888,
                          skydiverCategory: 'C',
                          personCategory: "Atleta");
      personAtleta04.save(failOnError: true)

      Location location = new Location(name: "Arcanjos, Torres/RS")
      location.save(failOnError: true)

      location = new Location(name: "Concorrente, Novo Hamburgo/RS")
      location.save(failOnError: true)

      Event event = new Event(
        name: "Saltos no Natal",
        startDate: new Date().parse('yyyy/MM/dd', "2017/12/23"),
        finishDate: new Date().parse('yyyy/MM/dd', "2017/12/24"),
        state: EventStateEnum.CLOSED)
      event.save(failOnError:true)

      Event eventCarnaval = new Event(
        name: "Saltos no Carnaval",
        startDate: new Date().parse('yyyy/MM/dd', "2018/02/10"),
        finishDate: new Date().parse('yyyy/MM/dd', "2018/02/11"),
        state: EventStateEnum.CLOSED)
      eventCarnaval.save(failOnError:true)

      def today = new Date()
      def yesterday = today - 1
      def nextDay = today + 1

      Event currentEvent = new Event(
        name: "Saltos entre ${yesterday.format('dd/MM/yyyy')} e ${nextDay.format('dd/MM/yyyy')}",
        startDate: yesterday,
        finishDate: nextDay,
        state: EventStateEnum.IN_PROGRESS)
      currentEvent.save(failOnError:true)

      event = new Event(
        name: "Saltos na Páscoa",
        startDate: new Date().parse('yyyy/MM/dd', "2018/03/31"),
        finishDate: new Date().parse('yyyy/MM/dd', "2018/04/01"),
        state: EventStateEnum.OPEN)
      event.save(failOnError:true)

      event = new Event(
        name: "Saltos na Pós Carnaval",
        startDate: new Date().parse('yyyy/MM/dd', "2018/02/17"),
        finishDate: new Date().parse('yyyy/MM/dd', "2018/02/18"),
        state: EventStateEnum.OPEN)
      event.save(failOnError:true)

      ManifestPerson manifestPersonTandemPassenger = new ManifestPerson(
        person: personPassageiroTandem,
        role: PersonRoleEnum.TANDEM_PASSENGER
        )

      ManifestPerson manifestPersonTandemPilot = new ManifestPerson(
        person: personAtleta03,
        role: PersonRoleEnum.TANDEM_PILOT,
        equipment: new Equipment(container: 'Vector', mainCanopySize: 400)
        )

      ManifestPerson manifestPersonCamera = new ManifestPerson(
        person: personAtleta04,
        role: PersonRoleEnum.CAMERAMAN,
        equipment: new Equipment(container: 'Vector', mainCanopySize: 170, owner: personAtleta03)
        )

      ManifestPerson manifestPersonBellyFly = new ManifestPerson(
        person: personAtleta01,
        role: PersonRoleEnum.SKYDIVER,
        equipment: new Equipment(container: 'Vector', mainCanopySize: 230, owner: personAtleta03)
        )

      def assistants = [manifestPersonTandemPilot, manifestPersonCamera]

      ManifestItem item1 = new ManifestItem(
          manifestPerson: manifestPersonTandemPassenger,
          jumpCategory: JumpCategoryEnum.TANDEM,
          observations: 'Salto tandem',
          assistants: assistants,
          calculatedValue: 830
        )      

      ManifestItem item2 = new ManifestItem(
          manifestPerson: manifestPersonBellyFly,
          jumpCategory: JumpCategoryEnum.BELLY_FLY,
          observations: 'Salto fun - belly'
        )

      def manifestItems = [item1, item2]

      Manifest manifest = new Manifest(
          creationDate: yesterday,
          aircraft: aircraftCessna,
          done: true,
          observation: 'Observação do 1º manifesto',
          items: manifestItems,
          eventId: currentEvent.id,
          altitude: 10000
        )
      manifest.save(failOnError:true)

      manifest = new Manifest(
          creationDate: today,
          aircraft: aircraftCessna,
          done: false,
          observation: 'O manifesto clone, segundo criado',
          items: manifestItems,
          eventId: currentEvent.id,
          altitude: 10000
        )
      manifest.save(failOnError:true)

      manifest = new Manifest(
          creationDate: today,
          aircraft: aircraft,
          done: false,
          observation: 'O manifesto clone, terceiro criado',
          items: manifestItems,
          eventId: currentEvent.id,
          altitude: 10000
        )
      manifest.save(failOnError:true)

    }
    def destroy = {
    }
}
