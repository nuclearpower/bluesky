<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main"/>
  <title><g:message code="location.plural.label"/></title>
</head>
<body>
  <location-table
    :locations="locations">
  </location-table>
</body>

</html>

