<template id="location-form-template">

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-12">
              <form v-on:submit.prevent role="form" @submit="saveLocation" @reset="clear">
                <div class="form-group">
                  <label><g:message code="location.name.label"/></label>
                  <input class="form-control" v-model="location.name">
                </div>
                <div class="footer-form-buttons">
                  <button type="submit" class="btn btn-default"><g:message code="form.save.label"/></button>
                  <button type="reset" class="btn btn-default"><g:message code="form.clear.label"/></button>
                </div>
              </form>
              <div class="form-message">
                <div
                  v-if="locationInsertDuplicated"
                  class="alert alert-warning">
                    <g:message code="location.form.duplicatedLocation"/>
                </div>
                <div
                  v-if="locationInsertFailed"
                  class="alert alert-danger">
                    <g:message code="location.form.insertLocationFailed"/>
                </div>
                <div
                  v-if="locationEditFailed"
                  class="alert alert-danger">
                    <g:message code="location.form.editLocationFailed"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</template>

<asset:javascript src="com/bluesky/location/location-form.js"/>
