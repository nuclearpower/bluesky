<template id="manifest-form-template">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-12">
              <form v-on:submit.prevent role="form" @submit="saveManifest" @reset="clearAllData">
                <div class="form-group col-lg-6">
                  <label><g:message code="manifest.form.aircraft.label"/></label>
                  <select class="form-control" v-model="manifest.aircraft">
                    <option v-for="aircraft in aircraftOptionsWithEmptyObj" :value="aircraft">
                      {{ aircraft.registration }} {{ aircraft.model }}
                    </option>
                  </select>
                </div>
                <div class="form-group col-lg-3">
                  <label><g:message code="manifest.form.altitude.label"/></label>
                  <input class="form-control" type="number" v-model="manifest.altitude">
                </div>
                <div class="form-group col-lg-3">
                  <label><g:message code="manifest.form.amount.passenger.label"/></label>
                  <input disabled class="form-control" type="number" v-model="totalPassengers">
                </div>
                <hr>
                <div>
                  <div class="form-group col-lg-3">
                    <label><g:message code="manifest.form.person.label"/></label>
                    <select class="form-control" v-model="personToInclude">
                      <option v-for="person in peopleOptionsWithEmptyObj" :value="person">
                        {{ getPassengerWithId(person) }}
                      </option>
                    </select>
                  </div>
                  <div class="form-group col-lg-2">
                    <label><g:message code="manifest.form.jumpcategory.label"/></label>
                    <select class="form-control" v-model="jumpCategoryToInclude">
                      <option v-for="option in filteredJumpCategoryOptions" :value="option">
                        {{ option.value }}
                      </option>
                    </select>
                  </div>
                  <div class="form-group col-lg-2">
                    <label><g:message code="manifest.form.equipment.container.label"/></label>
                    <span v-if="jumpCategoryToInclude != 'TANDEM'">
                      <autocomplete
                        :suggestions="containerSuggestions" 
                        :selection.sync="personContainerToInclude"
                        :allow-not-suggested-values="true"
                      >
                      </autocomplete>
                    </span>
                    <span v-else>
                      <input disabled class="form-control" type="text">
                    </span>
                  </div>

                  <div class="form-group col-lg-2">
                    <label><g:message code="manifest.form.equipment.main.canopy.label"/></label>
                    <span v-if="jumpCategoryToInclude != 'TANDEM'">
                      <input class="form-control" type="number" v-model="personMainCanopySizeToInclude">
                    </span>
                    <span v-else>
                      <input disabled class="form-control" type="text">
                    </span>
                  </div>
                  <div class="form-group col-lg-3">
                    <label><g:message code="manifest.form.locator.label"/></label>
                    <span v-if="jumpCategoryToInclude != 'TANDEM'">
                      <select class="form-control" v-model="locatorPersonEquipmentToInclude">
                        <option v-for="person in peopleOptionsWithEmptyObj" :value="person">
                          {{ person.name }}
                        </option>
                      </select>
                    </span>
                    <span v-else>
                      <select disabled class="form-control">
                      </select>
                    </span>
                  </div>
                  <div class="form-group col-lg-5">
                    <label><g:message code="manifest.form.instructor1.label"/></label>
                    <span v-if="amountInstructorsNeeded > 0">
                      <select class="form-control" v-model="instructor1ToInclude">
                        <option v-for="person in peopleOptionsWithEmptyObj" :value="person">
                          {{ getPassengerWithId(person) }}
                        </option>
                      </select>
                    </span>
                    <span v-else>
                      <select disabled class="form-control" v-model="instructor1ToInclude">
                      </select>
                    </span> 
                  </div>
                  <div class="form-group col-lg-2">
                    <label><g:message code="manifest.form.equipment.container.label"/></label>
                    <span v-if="amountInstructorsNeeded > 0">
                      <autocomplete
                        :suggestions="containerSuggestions" 
                        :selection.sync="instructor1ContainerToInclude"
                        :allow-not-suggested-values="true"
                      >
                      </autocomplete>
                    </span>
                    <span v-else>
                      <input disabled class="form-control" type="text" v-model="instructor1ContainerToInclude">
                    </span>
                  </div>
                  <div class="form-group col-lg-2">
                    <label><g:message code="manifest.form.equipment.main.canopy.label"/></label>
                    <span v-if="amountInstructorsNeeded > 0">
                      <input class="form-control" type="number" v-model="instructor1MainCanopySizeToInclude">
                    </span>
                    <span v-else>
                      <input disabled class="form-control" type="text" v-model="instructor1MainCanopySizeToInclude">
                    </span>
                  </div>
                  <div class="form-group col-lg-3">
                    <label><g:message code="manifest.form.locator.label"/></label>
                    <span v-if="amountInstructorsNeeded > 0">
                      <select class="form-control" v-model="locatorInstructor1EquipmentToInclude">
                        <option v-for="person in peopleOptionsWithEmptyObj" :value="person">
                          {{ person.name }}
                        </option>
                      </select>
                    </span>
                    <span v-else>
                      <select disabled class="form-control" v-model="locatorInstructor1EquipmentToInclude">
                      </select>
                    </span>
                  </div>
                  <div class="form-group col-lg-5">
                    <label><g:message code="manifest.form.instructor2.label"/></label>
                    <span v-if="amountInstructorsNeeded > 1">
                      <select class="form-control" v-model="instructor2ToInclude">
                        <option v-for="person in peopleOptionsWithEmptyObj" :value="person">
                          {{ getPassengerWithId(person) }}
                        </option>
                      </select>
                    </span>
                    <span v-else>
                      <select disabled class="form-control" v-model="instructor2ToInclude">
                      </select>
                    </span>
                  </div>
                  <div class="form-group col-lg-2">
                    <label><g:message code="manifest.form.equipment.container.label"/></label>
                    <span v-if="amountInstructorsNeeded > 1">
                      <autocomplete
                        :suggestions="containerSuggestions" 
                        :selection.sync="instructor2ContainerToInclude"
                        :allow-not-suggested-values="true"
                      >
                      </autocomplete>
                    </span>
                    <span v-else>
                      <input disabled class="form-control" type="text" v-model="instructor2ContainerToInclude">
                    </span>
                  </div>
                  <div class="form-group col-lg-2">
                    <label><g:message code="manifest.form.equipment.main.canopy.label"/></label>
                    <span v-if="amountInstructorsNeeded > 1">
                      <input class="form-control" type="number" v-model="instructor2MainCanopySizeToInclude">
                    </span>
                    <span v-else>
                      <input disabled class="form-control" type="text" v-model="instructor2MainCanopySizeToInclude">
                    </span>
                  </div>
                  <div class="form-group col-lg-3">
                    <label><g:message code="manifest.form.locator.label"/></label>
                    <span v-if="amountInstructorsNeeded > 1">
                      <select class="form-control" v-model="locatorInstructor2EquipmentToInclude">
                        <option v-for="person in peopleOptionsWithEmptyObj" :value="person">
                          {{ person.name }}
                        </option>
                      </select>
                    </span>
                    <span v-else>
                      <select disabled class="form-control" v-model="locatorInstructor2EquipmentToInclude">
                      </select>
                    </span>
                  </div>
                  <div class="form-group col-lg-5">
                    <label><g:message code="manifest.form.cameraman.label"/></label>
                    <select class="form-control" v-model="cameramanToInclude">
                      <option v-for="person in peopleOptionsWithEmptyObj" :value="person">
                        {{ getPassengerWithId(person) }}
                      </option>
                    </select>
                  </div>
                  <div class="form-group col-lg-2">
                    <label><g:message code="manifest.form.equipment.container.label"/></label>
                    <autocomplete
                      :suggestions="containerSuggestions" 
                      :selection.sync="cameramanContainerToInclude"
                      :allow-not-suggested-values="true"
                    >
                    </autocomplete>
                  </div>
                  <div class="form-group col-lg-2">
                    <label><g:message code="manifest.form.equipment.main.canopy.label"/></label>
                    <input class="form-control" type="number" v-model="cameramanMainCanopySizeToInclude">
                  </div>
                  <div class="form-group col-lg-3">
                    <label><g:message code="manifest.form.locator.label"/></label>
                    <select class="form-control" v-model="locatorCameramanEquipmentToInclude">
                      <option v-for="person in peopleOptionsWithEmptyObj" :value="person">
                        {{ person.name }}
                      </option>
                    </select>
                  </div>                  
                  <div class="form-group col-lg-4">
                    <label><g:message code="manifest.form.calculatedValue.label"/></label>
                    <input class="form-control" v-model="calculatedValue">
                  </div>
                  <div class="col-lg-2" style="text-align: center;">
                    <div v-if="affLevel" class="alert alert-info" style="padding-top: 6px; padding-bottom: 6px; margin-top: 20px; margin-bottom: 0px;">
                      {{ affLevel }}
                    </div>
                  </div>
                  <div class="col-lg-2">
                  </div>
                  <div class="form-group col-lg-4">
                    <label></label>
                    <div style="float: right; padding-top: 16px;">
                      <button type="button" @click="clearPassengerDataToInclude()" class="btn btn-default">Limpar</button>
                      <button type="button" @click="addPassenger()" class="btn btn-primary">Adicionar</button>
                    </div>
                  </div>
                </div>
                <hr>
                <b-container fluid>
                  <b-table 
                  striped hover 
                  stacked="md"
                  :current-page=1
                  :items="tableRows" 
                  :fields="tableFields"
                  style="font-size: 12px;">
                    <template slot="actions" slot-scope="row">
                        <i @click="removeManifestItem(row.item)"
                          class="fa fa-trash-o btn btn-primary">
                        </i>
                    </template>
                </b-table>
                </b-container>
                <div class="footer-form-buttons">
                  <button type="submit" class="btn btn-default"><g:message code="form.save.label"/></button>
              </form>
              <div class="form-message">
                <div
                  v-if="manifestInsertFailed"
                  class="alert alert-danger">
                    <g:message code="manifest.form.insertManifestFailed"/>
                </div>
                <div
                  v-if="manifestEditFailed"
                  class="alert alert-danger">
                    <g:message code="manifest.form.editManifestFailed"/>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
</template>

<asset:javascript src="com/bluesky/manifest/manifest-form.js"/>
