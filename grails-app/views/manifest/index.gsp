<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main"/>
  <title><g:message code="manifest.plural.label"/></title>
  <asset:stylesheet src="com/bluesky/manifest/manifest.css"/>
</head>
<body>
  <manifest-list 
    :manifests="manifests" 
    :current-event="currentEvent"
    :aircraft-options="aircrafts"
    :people-options="persons"
    :jump-category-options="jumpCategories">
  </manifest-list>
</body>

</html>
