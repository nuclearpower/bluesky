
<template id="manifest-template">

  <div class="manifest-card-summary" :class="manifestStyle">
    <span class="mainfest-order-label">
      #{{ visualOrderNumber }}
    </span>    
    <div style="float:right;">
      <i class="fa btn" :class="manifestStateStyle" @click="changeManifestStatus()"></i>
      <i class="fa fa-pencil btn" 
        @click="editManifest()"
        data-toggle="modal"
        data-target="#manifest-form">        
      </i>
      <i class="fa fa-trash-o btn" @click="removeManifest()"></i>      
    </div>
    <span @click="changeMode" style="cursor: pointer;">
      <component
        :is="mode"
        :manifest="manifest">
      </component>
    </span>
  </div>

</template>

<asset:javascript src="com/bluesky/manifest/manifest.js" />
