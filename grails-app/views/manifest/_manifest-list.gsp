
<template id="manifest-list-template">
  <div>
    <div class="manifest-list-header">
      <div class="manifest-list-add-button">
        <i
          @click="insertManifest"
          class="fa fa-plus-square btn btn-primary"
          data-toggle="modal"
          data-target="#manifest-form">
        </i>
      </div>
      <div class="btn-group manifest-list-period" data-toggle="buttons">
        <label class="btn btn-primary active" @click="setTodaySearch(true)">
          <input type="radio" name="selectedPeriod" id="today" autocomplete="off" checked> Hoje
        </label>
        <label class="btn btn-primary" @click="setTodaySearch(false)">
          <input type="radio" name="selectedPeriod" id="allPeriod" autocomplete="off" checked> Todos
        </label>
      </div>
      <div class="manifest-event-name">
        <label> <g:message code="event.singular.label"/>: {{ currentEvent.name }} </label>
      </div>
    </div>
    <div v-for="(manifest, index) in manifests">
      <manifest
        :manifest="manifest"
        :visual-order-number="manifests.length - index">
      </manifest>
      <br>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="manifest-form" tabindex="-1" role="dialog" aria-labelledby="manifestFormModalLabel" aria-hidden="true">
      <div class="modal-dialog manifest-modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button
              id="manifestFormClose"
              type="button"
              class="close"
              data-dismiss="modal"
              aria-hidden="true">&times;
            </button>
            <h4 class="modal-title" id="manifestFormModalLabel"><g:message code="manifest.form.title.label"/></h4>
          </div>
          <div class="modal-body">
            <manifest-form
              :aircraft-options="aircraftOptions"
              :people-options="peopleOptions"
              :current-event="currentEvent"
              :jump-category-options="jumpCategoryOptions">
            </manifest-form>
          </div>
        </div>
      </div>
    </div>

  </div>

</template>

<asset:javascript src="com/bluesky/manifest/manifest-list.js" />
