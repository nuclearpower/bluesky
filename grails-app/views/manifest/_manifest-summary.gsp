<template id="manifest-summary-template">

  <div>
    <span v-for="(manifestPerson, index) in involvedPeople">
      {{ extractFirstName(manifestPerson.person.name) }}
      <span v-if="index != involvedPeople.length - 1">,</span>
    </span>
    <span style="float: right; padding-right: 10px">
      Aeronave: {{ manifest.aircraft.registration }}
    </span>
  </div>
  
</template>

<asset:javascript src="com/bluesky/manifest/manifest-summary.js" />
