<template id="manifest-summary-expanded-template">

  <div>
    <p>
      <div>
        <span>
          Data de criação: {{ manifest.creationDate }}
        </span>
        <span style="float: right; padding-right: 10px">
          Aeronave: {{ manifest.aircraft.registration }}
        </span>
        <hr class="manifest-inner-separator">
      </div>
    </p>

    <p>
      <div v-for="item in formatManifestPersonList(manifest.items)">
        <pre class="manifest-item">{{ item }}</pre>
      </div>
    </p>

    <p class="manifest-observation">
      Obs.: {{ manifest.observation }}
    </p>
  </div>
  
</template>

<asset:javascript src="com/bluesky/manifest/manifest-summary-expanded.js" />
