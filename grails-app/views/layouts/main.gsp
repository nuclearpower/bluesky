<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />

    <!-- <asset:stylesheet src="application.css"/> -->

    <asset:stylesheet src="lib/bootstrap/css/bootstrap.min.css"/>
    <asset:stylesheet src="lib/bootstrap-vue/bootstrap-vue.min.css"/>

    <!-- MetisMenu CSS -->
    <asset:stylesheet src="lib/metisMenu/metisMenu.min.css"/>

    <!-- Custom CSS -->
    <asset:stylesheet src="lib/sb-admin-2/css/sb-admin-2.css"/>

    <!-- Morris Charts CSS -->
    <asset:stylesheet src="lib/morrisjs/morris.css"/>

    <!-- Custom Fonts -->
    <asset:stylesheet src="lib/font-awesome/css/font-awesome.min.css"/>

    <asset:stylesheet src="com/bluesky/bluesky.css"/>

    <g:layoutHead/>
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand app-title" href="index.html">Blue Sky</a>
      </div>
      <!-- /.navbar-header -->

      <ul class="nav navbar-top-links navbar-right">

        <!-- /.dropdown -->
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
            </li>
            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
            </li>
            <li class="divider"></li>
            <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
          </ul>
          <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
      </ul>
      <!-- /.navbar-top-links -->

      <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
          <ul class="nav" id="side-menu">            
            <li>
              <g:link controller="event">
                <i class="fa fa-calendar fa-fw"></i>
                <g:message code="event.plural.label"/>
              </g:link>
            </li>
            <li>
              <g:link controller="location">
                <i class="fa fa-map fa-fw"></i>
                <g:message code="location.plural.label"/>
              </g:link>
            </li>
            <li>
              <g:link controller="aircraft">
                <i class="fa fa-plane fa-fw"></i>
                <g:message code="aircraft.plural.label"/>
              </g:link>
            </li>
            <li>
              <g:link controller="person">
                <i class="fa fa-male fa-fw"></i>
                <g:message code="person.plural.label"/>
              </g:link>
            </li>
            <li>
              <g:link controller="defaultValues">
                <i class="fa fa-dollar fa-fw"></i> 
                <g:message code="defaultValues.form.title.label"/> 
              </g:link>
            </li>
            <li>
              <g:link controller="manifest">
                <i class="fa fa-hand-o-up fa-fw"></i> 
                <g:message code="manifest.plural.label"/>
              </g:link>
            </li>
          </ul>
        </div>
        <!-- /.sidebar-collapse -->
      </div>
      <!-- /.navbar-static-side -->
    </nav>
  </div>

    <asset:javascript src="lib/jquery/jquery.js"/>

    <asset:javascript src="lib/bootstrap/js/bootstrap.js"/>

    <asset:javascript src="lib/metisMenu/metisMenu.js"/>

    <asset:javascript src="lib/raphael/raphael.js"/>
    

    <asset:javascript src="lib/sb-admin-2/js/sb-admin-2.js"/>

    <asset:javascript src="lib/vuejs/vue.js"/>
    <asset:javascript src="lib/bootstrap-vue/bootstrap-vue.js"/>

    <g:render template="/components/autocomplete" />    
    <g:render template="/components/spinner" />

    <g:render template="/event/event-table" />
    <g:render template="/location/location-form" />
    <g:render template="/location/location-table" />
    
    <g:render template="/aircraft/aircraft-form" />
    <g:render template="/aircraft/container" />

    <g:render template="/person/person_form" />
    <g:render template="/person/person_table" />    

    <g:render template="/defaultValues/default-values-form" />

    <g:render template="/manifest/manifest-form" />
    <g:render template="/manifest/manifest-summary" />
    <g:render template="/manifest/manifest-summary-expanded" />
    <g:render template="/manifest/manifest" />
    <g:render template="/manifest/manifest-list" />

    <div id="page-wrapper">
      <div id="bluesky">        
        <g:layoutBody/>
      </div>
    </div>

    <div class="footer" role="contentinfo"></div>

    <div id="spinner" class="spinner" style="display:none;">
      <g:message code="spinner.alt" default="Loading&hellip;"/>
    </div>

    <script>
      var listEventsURL = "${createLink(controller:'Event', action:'list')}"
      var setCurrentEventURL = "${createLink(controller:'Event', action:'setCurrent')}"

      var listLocationsURL = "${createLink(controller:'Location', action:'list')}"
      var insertLocationURL = "${createLink(controller: 'Location', action:'insert')}"
      var editLocationURL = "${createLink(controller: 'Location', action:'edit')}"
      var removeLocationURL = "${createLink(controller: 'Location', action:'remove')}"
      
      var listAircraftsURL = "${createLink(controller:'Aircraft', action:'list')}"
      var insertAircraftURL = "${createLink(controller: 'Aircraft', action:'insert')}"
      var editAircraftURL = "${createLink(controller: 'Aircraft', action:'edit')}"
      var removeAircraftURL = "${createLink(controller: 'Aircraft', action:'remove')}"

      var listPersonsURL = "${createLink(controller:'Person', action:'list')}"
      var removePersonURL = "${createLink(controller:'Person', action:'remove')}"
      var updatePersonURL = "${createLink(controller:'Person', action:'save')}"
      var insertPersonURL = "${createLink(controller:'Person', action:'insert')}"

      var getDefaultValuesURL = "${createLink(controller: 'DefaultValues', action:'getDefaultValues')}"
      var updateDefaultValuesURL = "${createLink(controller: 'DefaultValues', action:'save')}"

      var getJumpCategoriesURL = "${createLink(controller: 'Manifest', action:'getJumpCategories')}"

      var confirmDeleteLabel = "${message(code:'confirm.delete.label')}"
      var typeToSearchLabel = "${message(code:'type.to.search.label')}"

      var eventNameLabel = "${message(code:'event.name.label')}"
      var eventStartDateLabel = "${message(code:'event.start.date.label')}"
      var eventFinishDateLabel = "${message(code:'event.finish.date.label')}"
      var eventStateLabel = "${message(code:'event.state.label')}"
      var eventStateOpenLabel = "${message(code:'event.state.enum.open.label')}"
      var eventStateInProgressLabel = "${message(code:'event.state.enum.inprogress.label')}"
      var eventStateClosedLabel = "${message(code:'event.state.enum.closed.label')}"

      var affLevelLabel = "${message(code:'aff.level.label')}"      

      var locationNameLabel = "${message(code:'location.name.label')}"
      
      var aircraftRegistrationLabel = "${message(code:'aircraft.registration.label')}"
      var aircraftModelLabel = "${message(code:'aircraft.model.label')}"
      var aircraftCapacityLabel = "${message(code:'aircraft.capacity.label')}"

      var personCpfLabel = "${message(code:'person.cpf.label')}"
      var personNameLabel="${message(code:'person.name.label')}"
      var personPhoneLabel = "${message(code:'person.phone.label')}"
      var personEmergencyPhoneLabel = "${message(code:'person.emergencyphone.label')}"
      var personWeightLabel = "${message(code:'person.weight.label')}"
      var personCbpqRegistrationLabel = "${message(code:'person.cbpqregistration.label')}"

      var personRoleCameramanLabel = "${message(code:'person.role.enum.cameraman.label')}"
      var personRoleInstructorLabel = "${message(code:'person.role.enum.instructor.label')}"
      var personRoleTandemPilotLabel = "${message(code:'person.role.enum.tandemPilot.label')}"
      var personRoleTandemPassengerLabel = "${message(code:'person.role.enum.tandemPassenger.label')}"
      var personRoleSkydiverLabel = "${message(code:'person.role.enum.skydiver.label')}"

      var manifestListURL = "${createLink(controller: 'Manifest', action:'list')}"
      var insertManifestURL = "${createLink(controller: 'Manifest', action:'insert')}"
      var editManifestURL = "${createLink(controller: 'Manifest', action:'edit')}"
      var removeManifestURL = "${createLink(controller: 'Manifest', action:'remove')}"
      var setManifestDoneURL = "${createLink(controller: 'Manifest', action:'setManifestDone')}"
    </script>

    <asset:javascript src="com/bluesky/mixin/event-state-mixin.js"/>

    <asset:javascript src="com/bluesky/main.js" />

</body>
</html>
