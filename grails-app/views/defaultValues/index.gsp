<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main"/>
  <title><g:message code="defaultValues.form.title.label"/></title>
</head>
<body>
  <default-values-form
    :default-values="defaultValues">
  </default-values-form>
</body>

</html>
