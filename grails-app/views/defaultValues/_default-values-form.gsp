<template id="default-values-form-template">

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-4">
              <form v-on:submit.prevent role="form" @submit="saveDefaultValues" @reset="clear">
                <div class="form-group">
                  <label><g:message code="defaultValues.form.takeOff.label"/></label>
                  <input class="form-control" v-model="defaultValues.takeOff">
                </div>
                <div class="form-group">
                  <label><g:message code="defaultValues.form.tandemSkydive.label"/></label>
                  <input class="form-control" v-model="defaultValues.tandemSkydive">
                </div>
                <div class="form-group">
                  <label><g:message code="defaultValues.form.parachutePacking.label"/></label>
                  <input class="form-control" v-model="defaultValues.parachutePacking">
                </div>
                <div class="form-group">
                  <label><g:message code="defaultValues.form.equipmentRental.label"/></label>
                  <input class="form-control" v-model="defaultValues.equipmentRental">
                </div>
                <div class="footer-form-buttons">
                  <button type="submit" class="btn btn-default"><g:message code="form.save.label"/></button>
                  <button type="reset" class="btn btn-default"><g:message code="form.clear.label"/></button>
                </div>
              </form>
              <div class="form-message">
                <div
                  v-if="defaultValuesEditFailed"
                  class="alert alert-danger">
                    <g:message code="defaultValues.form.editDefaultValuesFailed"/>
                </div>
                <div
                  v-if="defaultValuesEditSucceed"
                  class="alert alert-success">
                    <g:message code="defaultValues.form.editDefaultValuesSucceed"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</template>

<asset:javascript src="com/bluesky/default-values/default-values-form.js"/>
