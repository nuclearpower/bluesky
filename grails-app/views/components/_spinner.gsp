<template id="spinner-template">
  <div class="input-group number-spinner">
    <span class="input-group-btn">
      <button type="button" class="btn btn-default" @click="decrease()"><span class="glyphicon glyphicon-minus"></span></button>
    </span>
    <input readonly type="text" class="form-control text-center" v-model="value">
    <span class="input-group-btn">
      <button type="button" class="btn btn-default" @click="increase()"><span class="glyphicon glyphicon-plus"></span></button>
    </span>
  </div>
</template>

<asset:javascript src="com/bluesky/components/spinner.js" />
