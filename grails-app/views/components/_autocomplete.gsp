<template id="autocomplete-template">
  <div style='position:relative' v-bind:class="{'open':openSuggestion}">
    <input class="form-control" type="text" v-model="selection"
      @keydown.enter = 'enter'
      @blur = 'enter'
      @keydown.down = 'down'
      @keydown.up = 'up'
      @input = 'change'/>
    <ul class="dropdown-menu" style="width:100%">
      <li v-for="(suggestion, index) in matches" 
        v-bind:class="{'active': isActive(index)}"
        @click="suggestionClick(index)"
      >
        <a href="#">{{ suggestion }}</a>
      </li>
    </ul>
  </div>
</template>

<asset:javascript src="com/bluesky/components/autocomplete.js" />
