<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main"/>
  <title><g:message code="event.plural.label"/></title>
</head>
<body>
  <event-table
    :events="events">
  </event-table>
</body>

</html>
