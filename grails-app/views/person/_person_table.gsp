<asset:stylesheet src="com/bluesky/person/table.css" />

<template id="person-table-template">
  <b-container fluid>
    <b-row>
      <b-col md="6">
        <b-form-group style="margin-top: 10px; float:left;">
          <b-input-group>
            <i @click="selectPerson()" 
            class="fa fa-plus-square btn btn-primary" 
            data-toggle="modal" 
            data-target="#myModal"></i>
          </b-input-group>
        </b-form-group>
      </b-col>
      <b-col md="6">
        <b-form-group style="margin-top: 10px; float:right;">
          <b-input-group>
            <b-form-input v-model="filter" :placeholder="typeToSearchLabel"/>
          </b-input-group>
        </b-form-group>
      </b-col>
    </b-row>    
    <b-table hover 
      stacked="md"
      :current-page="currentPage"
      :items="persons" 
      :fields="fields"
      :filter="filter"
      :per-page="perPage"
      :sort-by.sync="sortBy"
      :sort-desc.sync="sortDesc"
      @filtered="onFiltered"
      >
      <template slot="personCategory" slot-scope="row">
        <i :class="personCategoryClass(row.item.personCategory)" data-toggle="tooltip" :title="row.item.personCategory"></i>
      </template>
      <template slot="actions" slot-scope="row">
        <div class="person-table-buttons">
        <i @click="selectPerson(row.item)" 
            class="fa fa-pencil btn btn-primary" 
            data-toggle="modal" 
            data-target="#myModal"></i>
        <i @click="removePerson(row.item)" class="fa fa-trash-o btn btn-primary"></i>
      </div>
      </template>
    </b-table>
    <b-row style="margin-top: -25px;">
      <b-col md="12">
        <b-pagination 
          :total-rows="totalRows" 
          :per-page="perPage" 
          v-model="currentPage" 
          style="float: right; position: relative; left: -50%; /* or right 50% */"
          />
      </b-col>
    </b-row>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" 
                class="close" 
                data-dismiss="modal" 
                aria-hidden="true"
                id="closeUpdatePerson">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Cadastro de pessoa</h4>
          </div>
          <div class="modal-body">
            <person-form :person="selectedPerson">
            </person-form>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    
  </b-container fluid>
</template>

<asset:javascript src="com/bluesky/person/person-table.js" />

