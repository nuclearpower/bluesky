
<template id="person-form-template">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-12">
              <form role="form" @submit="savePerson" v-on:submit.prevent>
                <div class="form-group col-lg-6">
                  <label><g:message code="person.name.label"/></label>
                  <input class="form-control" v-model="person.name">
                </div>
                <div class="form-group col-lg-6">
                  <label><g:message code="person.cpf.label"/></label>                  
                  <input class="form-control" v-model="person.cpf" :readonly="!insertMode">
                </div>
                <div class="form-group col-lg-6">
                  <label><g:message code="person.phone.label"/></label>
                  <input class="form-control" v-model="person.phoneNumber">
                </div>
                <!-- <div class="form-group col-lg-4">
                  <label><g:message code="person.birthdate.label"/></label>
                  <input class="form-control" v-model="person.birthDate">                
                </div> -->
                <div class="form-group col-lg-6">
                  <label><g:message code="person.emergencyphone.label"/></label>
                  <input class="form-control" v-model="person.emergencyPhoneNumber">
                </div>
                <div class="form-group col-lg-4">
                  <label><g:message code="person.bloodgroupandrhfactor.label"/></label>
                  <select class="form-control" v-model="person.bloodGroupAndRhFactor">
                    <option>A+</option>
                    <option>A-</option>
                    <option>B+</option>
                    <option>B-</option>
                    <option>AB+</option>
                    <option>AB-</option>
                    <option>O+</option>
                    <option>O-</option>
                  </select>
                </div>
                <div class="form-group col-lg-4">
                  <label><g:message code="person.weight.label"/></label>
                  <input class="form-control" v-model="person.weight">
                </div>
                <div class="form-group col-lg-4">
                  <label><g:message code="person.category.label"/></label>
                  <select class="form-control" v-model="person.personCategory">
                    <option>Aluno</option>
                    <option>Atleta</option>
                    <option>Outro</option>
                    <option>Passageiro Tandem</option>
                  </select>
                </div>                
                <div v-if="isAthlete || isStudent">
                  <div class="form-group col-lg-6">
                    <label><g:message code="person.cbpqregistration.label"/></label>
                    <input class="form-control" v-model="person.cbpqRegistration">
                  </div>
                  <div class="form-group col-lg-6">
                    <label><g:message code="person.skydivercategory.label"/></label>
                    <select class="form-control" v-model="person.skydiverCategory">
                      <option>AI</option>
                      <option>A</option>
                      <option>B</option>
                      <option>C</option>
                      <option>D</option>
                    </select>
                  </div>
                </div>
                <div v-if="isStudent" class="form-group col-lg-6">
                  <label><g:message code="person.studentCourseType.label"/></label>
                  <select class="form-control" v-model="person.studentCourseType">
                    <option>ASL</option>
                    <option>AFF</option>
                  </select>
                </div>
                <div v-if="isAffStudent" class="form-group col-lg-6">
                  <label><g:message code="person.studentAffLevel.label"/></label>
                  <spinner :min-value="1" :max-value="7" :value.sync="person.studentAffLevel">
                  </spinner>
                </div>
                <div class="form-group col-lg-12">
                  <label><g:message code="person.medicalobservations.label"/></label>
                  <textarea class="form-control" rows="3" v-model="person.medicalObservations"></textarea>
                </div>
                <div class="form-group col-lg-12 footer-form-buttons">
                  <button type="submit" class="btn btn-default">Salvar</button>
                  <button type="reset" class="btn btn-default">Limpar</button>
                </div>
              </form>              
            </div>            
          </div>
          <!-- /.row (nested) -->
          <div class="form-message row">
              <div
                v-if="personInsertDuplicated"
                class="alert alert-danger">
                <g:message code="person.form.duplicatedPerson"/>
              </div>
              <div
                v-if="personInsertFailed"
                class="alert alert-danger">
                <g:message code="person.form.insertPersonFailed"/>
              </div>
              <div
                v-if="personOperationFailed"
                class="alert alert-danger">
                <g:message code="person.form.operationFailed"/>
              </div>
            </div>
        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</template>

<asset:javascript src="com/bluesky/person/person-form.js"/>
