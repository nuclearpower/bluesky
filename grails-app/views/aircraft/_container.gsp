<asset:stylesheet src="com/bluesky/aircraft/container.css" />

<template id="aircraft-container-template">

  <b-container fluid>
    <b-row>
      <b-col md="6">
        <b-form-group style="margin-top: 10px; float:left;">
          <b-input-group>
            <i
              @click="insertAircraft"
              class="fa fa-plus-square btn btn-primary"
              data-toggle="modal"
              data-target="#aircraft-form">
            </i>
          </b-input-group>
        </b-form-group>
      </b-col>
      <b-col md="6">
        <b-form-group style="margin-top: 10px; float:right;">
          <b-input-group>
            <b-form-input v-model="filter" :placeholder="typeToSearchLabel"/>
          </b-input-group>
        </b-form-group>
      </b-col>
    </b-row>
    <b-table hover 
      stacked="md"
      :current-page="currentPage"
      :items="aircrafts" 
      :fields="fields"
      :filter="filter"
      :per-page="perPage"
      :sort-by.sync="sortBy"
      :sort-desc.sync="sortDesc"
      @filtered="onFiltered">
        <template slot="actions" slot-scope="row">
            <i @click="editAircraft(row.item)" 
              class="fa fa-pencil btn btn-primary" 
              data-toggle="modal" 
              data-target="#aircraft-form">
            </i>
            <i @click="removeAircraft(row.item)"
              class="fa fa-trash-o btn btn-primary">
            </i>
        </template>
    </b-table>
    <b-row style="margin-top: -25px;">
      <b-col md="12">
        <b-pagination 
        :total-rows="totalRows" 
        :per-page="perPage" 
        v-model="currentPage" 
        style="float: right; position: relative; left: -50%; /* or right 50% */"
        />
      </b-col>
    </b-row>

    <!-- Modal -->
    <div class="modal fade" id="aircraft-form" tabindex="-1" role="dialog" aria-labelledby="aircraftFormModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button
              id="aircraftFormClose"
              type="button"
              class="close"
              data-dismiss="modal"
              aria-hidden="true">&times;
            </button>
            <h4 class="modal-title" id="aircraftFormModalLabel"><g:message code="aircraft.form.title.label"/></h4>
          </div>
          <div class="modal-body">
            <aircraft-form>
            </aircraft-form>
          </div>
        </div>
      </div>
    </div>
  </b-container fluid>

</template>

<asset:javascript src="com/bluesky/aircraft/container.js" />

