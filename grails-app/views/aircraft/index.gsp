<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main"/>
  <title><g:message code="aircraft.plural.label"/></title>
</head>
<body>
  <aircraft-container
    :aircrafts="aircrafts">
  </aircraft-container>
</body>

</html>

