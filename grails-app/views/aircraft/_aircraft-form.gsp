<template id="aircraft-form-template">

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-12">
              <form v-on:submit.prevent role="form" @submit="saveAircraft" @reset="clear">
                <div class="form-group">
                  <label><g:message code="aircraft.registration.label"/></label>
                  <input class="form-control" v-model="aircraft.registration" :readonly="editMode">
                </div>
                <div class="form-group">
                  <label><g:message code="aircraft.model.label"/></label>
                  <input class="form-control" v-model="aircraft.model">
                </div>
                <div class="form-group">
                  <label><g:message code="aircraft.capacity.label"/></label>
                  <input type="number" class="form-control" v-model="aircraft.capacity">
                </div> 
                <div class="footer-form-buttons">
                  <button type="submit" class="btn btn-default"><g:message code="form.save.label"/></button>
                  <button type="reset" class="btn btn-default"><g:message code="form.clear.label"/></button>
                </div>
              </form>
              <div class="form-message">
                <div
                  v-if="aircraftInsertDuplicated"
                  class="alert alert-warning">
                    <g:message code="aircraft.form.duplicatedAircraft"/>
                </div>
                <div
                  v-if="aircraftInsertFailed"
                  class="alert alert-danger">
                    <g:message code="aircraft.form.insertAircraftFailed"/>
                </div>
                <div
                  v-if="aircraftEditFailed"
                  class="alert alert-danger">
                    <g:message code="aircraft.form.editAircraftFailed"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</template>

<asset:javascript src="com/bluesky/aircraft/aircraft-form.js"/>
