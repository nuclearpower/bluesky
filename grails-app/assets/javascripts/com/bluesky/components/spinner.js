var vmSpinner = {
  template: "#spinner-template",

  props: {
    minValue: {
      type: Number,
      required: false,
      default: 0
    },
    maxValue: {
      type: Number,
      required: false,
      default: 2147483647 //maximum integer value in Java
    },
    value: {
      type: Number,
      twoWay: true
    }
  },

  beforeMount: function() {
    this.$emit('update:value', this.minValue)
  },

  methods: {
    increase: function() {
      if ((this.maxValue) && (this.value < this.maxValue)) {
        this.$emit('update:value', this.value + 1)
      }
    },
    decrease: function() {
      if (this.value > this.minValue) {
        this.$emit('update:value', this.value - 1)
      }
    }
  }
}
