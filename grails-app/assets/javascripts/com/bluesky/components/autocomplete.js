// Credits to http://fareez.info/blog/create-your-own-autocomplete-using-vuejs/

var vmAutocomplete = {

  template: "#autocomplete-template",

  props: {
    suggestions: {
      type: Array,
      required: true
    },

    selection: {
      type: String,
      required: true,
      twoWay: true
    },

    allowNotSuggestedValues: {
      type: Boolean,
      default: false
    }
  },

  data: function() {
    return {
        open: false,
        current: 0
    }
  },

  computed: {

    //Filtering the suggestion based on the input
    matches() {
      if (this.selection) {
        return this.suggestions.filter((str) => {
          return str.toLowerCase().indexOf(this.selection.toLowerCase()) >= 0;
        });
      } else {
        return []
      } 
    },

    //The flag
    openSuggestion() {
      return this.selection !== "" &&
            this.matches.length != 0 &&
            this.open === true;
    }
  },

  methods: {
    //When enter pressed on the input
    enter(event) {
      if (event) event.preventDefault()
      var selection = this.matches[this.current]
      if ((this.allowNotSuggestedValues) && (!selection)) {
        selection = this.selection
      }
      if (selection) {
        this.$emit('update:selection', selection)
      }
      this.open = false;
    },

    //When up pressed while suggestions are open
    up() {
      if(this.current > 0)
        this.current--;
    },

    //When up pressed while suggestions are open
    down() {
      if(this.current < this.matches.length - 1)
        this.current++;
    },

    //For highlighting element
    isActive(index) {
      return index === this.current;
    },

    //When the user changes input
    change() {
      if (this.open == false) {
        this.open = true;
        this.current = 0;
      }
    },

    //When one of the suggestion is clicked
    suggestionClick(index) {
      this.$emit('update:selection', this.matches[index])
      this.open = false;
    }
  },

}
