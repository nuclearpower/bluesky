var MAX_POST_TRIES = 3

var eventBus = new Vue();

Vue.mixin(eventStateMixin)

var vmVue = new Vue({
  
  el : "#bluesky",

  data : {
    title: "bluesky",
    events: [],
    currentEvent: {
      id: -1,
      name: "####"
    },
    locations: [],
    aircrafts: [],
    persons: [],
    defaultValues: {},
    manifests: [],

    jumpCategories: []
  },

  components : {
    "event-table" : vmEventTable,
    "location-table" : vmLocationTable,
    "aircraft-container" : vmAircraftContainer,
    "person-table" : vmPersonTable,
    "default-values-form": vmDefaultValuesForm,
    "manifest-list" : vmManifestList
  },

  created : function() {
    var self = this

    $.post(listEventsURL)
    .done(function(data) {          
      self.events = data
      self.currentEvent = self.getCurrentEvent()
      self.loadManifests(true)
    })
    .fail(function() {
      console.log("ERROR")
    })

    $.post(listLocationsURL)
    .done(function(data) {          
      self.locations = data
    })
    .fail(function() {
      console.log("ERROR")
    })

    $.post(listAircraftsURL)
    .done(function(data) {          
      self.aircrafts = data
    })
    .fail(function() {
      console.log("ERROR")
    })

    $.post(listPersonsURL)
    .done(function(data) {          
      self.persons = data
    })
    .fail(function() {
      console.log("ERROR")
    })

    $.post(getDefaultValuesURL)
    .done(function(data) {
      self.defaultValues = data      
    })
    .fail(function() {
      console.log("ERROR")
    })

    $.post(getJumpCategoriesURL)
    .done(function(data) {          
      self.jumpCategories = data
    })
    .fail(function() {
      console.log("ERROR")
    })
    
  },

  mounted: function() {
    var self = this;
    eventBus.$on('removePerson', function(person) {
      $.post(removePersonURL, {cpf: person.cpf})
      .done(function(data) {      
        self.removePerson(person);
      })
      .fail(function() {
        console.log("ERROR")
      })
    });
    eventBus.$on('personUpdated', function(person) { 
      var indexOf = -1
      self.persons.some(function(childPerson, index) {      
        var result = childPerson.cpf == person.cpf
        if (result) {
          indexOf = index
        }
        return result
      })
      if (indexOf != -1) {
        self.persons.splice(indexOf, 1, person);
      }
    });

    eventBus.$on("includeLocation", function(location) {
      self.includeLocation(location)
    })

    eventBus.$on("editLocationSubmit", function(location) {
      self.editLocation(location)
    })

    eventBus.$on("removeLocation", function(location) {
      self.removeLocation(location)
    })

    eventBus.$on("includeAircraft", function(aircraft) {
      self.includeAircraft(aircraft)
    })

    eventBus.$on("editAircraftSubmit", function(aircraft) {
      self.editAircraft(aircraft)
    })

    eventBus.$on("removeAircraft", function(aircraft) {
      self.removeAircraft(aircraft)
    })

    eventBus.$on('defaultValuesUpdated', function(defaultValues) {
      self.defaultValues = defaultValues;
    })

    eventBus.$on("setCurrentEvent" , function(currentEvent) {
      self.setCurrentEvent(currentEvent)
      self.loadManifests(true)
    })

    eventBus.$on("includeManifest", function(manifest) {
      self.includeManifest(manifest)
    })

    eventBus.$on("editManifestSubmit", function(manifest) {
      self.editManifest(manifest)
    })

    eventBus.$on("removeManifest", function(manifest) {
      self.removeManifest(manifest)
    })

    eventBus.$on("changedManifestPeriod", function(isToday) {
      self.loadManifests(isToday)      
    })
  },

  methods : {
    removePerson: function(person) {
      const index = this.persons.indexOf(person);
      if (index !== -1) {
        this.persons.splice(index, 1);        
      }
    },

    includeAircraft : function(aircraft) {

      var self = this
      var aircraftJSON = JSON.stringify(aircraft)
      $.post(insertAircraftURL, {aircraft : aircraftJSON})
      .done(function(data) {
        switch(data) {
          case "InsertDuplicated" :
            eventBus.$emit("aircraftInsertDuplicated")
            break
          case "InsertFailed" :
            eventBus.$emit("aircraftInsertFailed")
            break
          case "Inserted" :
            self.aircrafts.push(aircraft)
            eventBus.$emit("aircraftInserted")
            break
        }
        eventBus.$emit("")
      })
      .fail(function(data) {
        eventBus.$emit("aircraftInsertFailed")
      })
    },

    editAircraft : function(aircraft) {
      var self = this
      var aircraftJSON = JSON.stringify(aircraft)
      $.post(editAircraftURL, {aircraft:aircraftJSON})
      .done(function(data) {
        switch(data) {
          case "EditedFailed" :
            eventBus.$emit("aircraftEditFailed")
            break
          case "Edited" :
            self.onAircraftEdited(aircraft)
            eventBus.$emit("aircraftEdited")
            break
        }
      })
      .fail(function() {
        eventBus.$emit("aircraftEditFailed")
      })
    },

    onAircraftEdited : function(aircraft) {
      this.aircrafts.some(function(childAircraft) {
        var edited = childAircraft.registration == aircraft.registration
        if(edited) {
          childAircraft.model = aircraft.model
          childAircraft.capacity = aircraft.capacity
        }
        return edited
      })
    },

    removeAircraft : function(aircraft, tryNumber) {
      var self = this
      tryNumber = tryNumber ? tryNumber : 1
      $.post(removeAircraftURL, {registration: aircraft.registration})

      .done(function(data) {
        var removeIndex = self.aircrafts.indexOf(aircraft)
        if(removeIndex !== -1) {
          self.aircrafts.splice(removeIndex, 1)
        }
      })
      .fail(function() {
          if(tryNumber < MAX_POST_TRIES) {
            self.removeAircraft(aircraft, ++tryNumber)
          } else {
            console.log("AIRCRAFT REMOVED ERROR!")
          }
      })
    },

    includeLocation : function(location) {

      var self = this
      var locationJSON = JSON.stringify(location)
      $.post(insertLocationURL, {location : locationJSON})
      .done(function(data) {
        switch(data) {
          case "InsertDuplicated" :
            eventBus.$emit("locationInsertDuplicated")
            break
          case "InsertFailed" :
            eventBus.$emit("locationInsertFailed")
            break
          default :
            location.id = data.id
            location.name = data.name
            self.locations.push(location)
            eventBus.$emit("locationInserted")
            break
        }
        eventBus.$emit("")
      })
      .fail(function(data) {
        eventBus.$emit("locationInsertFailed")
      })
    },

    editLocation : function(location) {
      var self = this
      var locationJSON = JSON.stringify(location)
      $.post(editLocationURL, {location:locationJSON})
      .done(function(data) {
        switch(data) {
          case "EditedFailed" :
            eventBus.$emit("locationEditFailed")
            break
          case "Edited" :
            self.onLocationEdited(location)
            eventBus.$emit("locationEdited")
            break
        }
      })
      .fail(function() {
        eventBus.$emit("locationEditFailed")
      })
    },

    onLocationEdited : function(location) {
      this.locations.some(function(childLocation) {
        var edited = childLocation.id == location.id
        if(edited) {
          childLocation.name = location.name
        }
        return edited
      })
    },

    removeLocation : function(location, tryNumber) {
      var self = this
      tryNumber = tryNumber ? tryNumber : 1
      $.post(removeLocationURL, {id: location.id})

      .done(function(data) {
        var removeIndex = self.locations.indexOf(location)
        if(removeIndex !== -1) {
          self.locations.splice(removeIndex, 1)
        }
      })
      .fail(function() {
          if(tryNumber < MAX_POST_TRIES) {
            self.removeLocation(location, ++tryNumber)
          } else {
            console.log("LOCATION REMOVED ERROR!")
          }
      })
    },

    includeManifest: function(manifest) {

      var self = this
      var manifestJSON = JSON.stringify(manifest)
      $.post(insertManifestURL, {manifest : manifestJSON})
      .done(function(data) {
        switch(data) {
          case "InsertFailed" :
            eventBus.$emit("manifestInsertFailed")
            break
          default :
            self.manifests.unshift(data)
            eventBus.$emit("manifestInserted")
            break
        }
        eventBus.$emit("")
      })
      .fail(function(data) {
        eventBus.$emit("manifestInsertFailed")
      })
    },

    editManifest : function(manifest) {
      var self = this      
      var manifestJSON = JSON.stringify(manifest)
      $.post(editManifestURL, {manifest: manifestJSON})
      .done(function(data) {
        switch(data) {
          case "EditedFailed" :
            eventBus.$emit("manifestEditFailed")
            break
          default :
            self.onManifestEdited(data)
            eventBus.$emit("manifestEdited", data)
            break
        }
      })
      .fail(function() {
        eventBus.$emit("manifestEditFailed")
      })
    },

    onManifestEdited : function(manifest) {
      var self = this      
      this.manifests.some(function(childManifest, index) {
        var edited = childManifest.id == manifest.id
        if (edited) {
          childManifest = manifest
        }
        return edited
      })
    },

    removeManifest : function(manifest, tryNumber) {
      var self = this
      tryNumber = tryNumber ? tryNumber : 1
      $.post(removeManifestURL, {id: manifest.id})

      .done(function(data) {
        var removeIndex = self.manifests.indexOf(manifest)
        if(removeIndex !== -1) {
          self.manifests.splice(removeIndex, 1)
        }
      })
      .fail(function() {
          if(tryNumber < MAX_POST_TRIES) {
            self.removeLocation(manifest, ++tryNumber)
          } else {
            console.log("MANIFEST REMOVED ERROR!")
          }
      })
    },

    loadManifests: function(isToday) {
      var self = this
      $.post(manifestListURL, {currentEventId: self.currentEvent.id, today: isToday})
      .done(function(data) {
        self.manifests = data
        eventBus.$emit("updatedManifestList", self.manifests)
      })
      .fail(function() {
        console.log("ERROR LOADING MANIFESTS")
      })
    },

    getCurrentEvent : function() {
      var self = this
      var result = {id: -1, name: "####"}
      this.events.some(function(event) {
        var thereIsEventInProgress = self.isInProgress(event.state)
        if(thereIsEventInProgress) {
          result = event
        }
        return thereIsEventInProgress
      })
      return result;
    },

    setCurrentEvent : function(currentEvent) {
      var self = this
      $.post(setCurrentEventURL, {eventId: currentEvent.id})
      .done(function(data) {

        self.events.map(function(event) {
          if(event.id == currentEvent.id) {
            event.state = self.getState("IN_PROGRESS")
            self.currentEvent = event
          } else if(event.state.name == "IN_PROGRESS") {
            event.state = self.getState("OPEN")
          }
        })
      })
    }
  }
})
