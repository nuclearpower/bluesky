var vmManifest = {

  template : "#manifest-template",

  props : [
    "manifest", 
    "visualOrderNumber"
  ],

  data : function() {
    return {
      mode : "manifest-summary"
    }
  },

  computed: {
    manifestStyle: function() {
      var styleClass = "w3-light-blue"
      if (!this.manifest.done) {
        styleClass = "w3-gray"
      }
      return styleClass
    },
    manifestStateStyle: function() {
      var styleClass = "fa-check"
      if (this.manifest.done) {
        styleClass = "fa-undo"
      }
      return styleClass
    }
  },

  components : {
    "manifest-summary" : vmManifestSummary,
    "manifest-summary-expanded" : vmManifestSummaryExpanded
  },

  methods: {
    changeMode: function() {
      switch(this.mode) {
        case 'manifest-summary':
          this.mode = 'manifest-summary-expanded'
          break
        case 'manifest-summary-expanded':
          this.mode = 'manifest-summary'
          break
      }
    },

    changeManifestStatus: function(tryNumber) {
      var newStatus = !this.manifest.done
      var self = this
      tryNumber = tryNumber ? tryNumber : 1
      $.post(setManifestDoneURL, {id: this.manifest.id, done: newStatus})
      .done(function(data) {
        self.manifest.done = newStatus
      })
      .fail(function() {
          if(tryNumber < MAX_POST_TRIES) {
            self.changeManifestStatus(++tryNumber)
          } else {
            console.log("MANIFEST STATUS CHANGE ERROR!")
          }
      })
    },

    removeManifest : function() {
      if (confirm(confirmDeleteLabel)) {
        eventBus.$emit("removeManifest", this.manifest)
      }
    },

    editManifest : function() {
      eventBus.$emit("editManifestStart", $.extend({}, this.manifest))
    },
  }
}
