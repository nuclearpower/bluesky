var vmManifestSummary = {

  template : "#manifest-summary-template",

  props : ["manifest"],

  data : function() {
    return {
      involvedPeople : [],
    }
  },

  beforeMount : function() {
    var self = this
    this.manifest.items.map(function(item) {
        
        self.involvedPeople.push(item.manifestPerson)

        item.assistants.map(function(manifestPerson) {
          self.involvedPeople.push(manifestPerson)
        })
    })
  },

  methods: {
    extractFirstName: function(name) {
      return name.slice(0, name.indexOf(' '))
    }
  }
}
