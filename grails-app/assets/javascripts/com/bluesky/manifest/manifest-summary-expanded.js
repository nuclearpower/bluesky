var vmManifestSummaryExpanded = {

  template : "#manifest-summary-expanded-template",

  props : ["manifest"],

  methods : {

    getMergedEquipmentData: function(container, mainCanopySize, locator) {
      var result = ""
      if (container) {
        result +=  " - " 
        result += container
        if (mainCanopySize) {
          result += " " + mainCanopySize
        }
        if ((locator) && (locator.name)) {
          result +=  " (" + locator.name + ")"
        }
      }
      return result
    },

    formatManifestPersonList: function(items) {
      var listManifestPerson = []
      var index = 0
      var self = this
      items.map(function(item) {
        var resultString = ""
        index++
        resultString += index + ". "
        resultString += item.manifestPerson.person.name
        resultString += " - " + item.jumpCategory.value
        resultString += self.getMergedEquipmentData(
            item.manifestPerson.equipment.container,
            item.manifestPerson.equipment.mainCanopySize,
            item.manifestPerson.equipment.owner
          )
        listManifestPerson.push(resultString)        
        item.assistants.map(function(assistant) {          
          resultString = ""
          index++
          resultString += index + ". "
          resultString += new Array(4).join(" ")
          resultString += assistant.person.name
          resultString += " - " + assistant.role.value
          resultString += self.getMergedEquipmentData(
            assistant.equipment.container,
            assistant.equipment.mainCanopySize,
            assistant.equipment.owner
          )
          listManifestPerson.push(resultString)
        })
      })
      return listManifestPerson
    }
  }

}
