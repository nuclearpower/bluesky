var vmManifestForm = {
  template: "#manifest-form-template",

  props: [
    "aircraftOptions",
    "peopleOptions",
    "currentEvent",
    "jumpCategoryOptions"
  ],

  components: {
    "autocomplete": vmAutocomplete
  },

  data: function() {
    return {
      manifest: {
          aircraft: {},
          eventId: -1,
          altitude: "",
          items: [],
          id: -1
      },
      manifestItemsDenormalized: [],
      tableFields: {
        person: { label: 'Pessoa', sortable: true },
        jumpCategory: { label: 'Tipo', sortable: true },
        personEquipment: {label: 'Equipamento', sortable: true},
        instructor1: { label: 'Instrutor 1', sortable: true },
        instructor1Equipment: {label: 'Equipamento', sortable: true},
        instructor2: { label: 'Instrutor 2', sortable: true },
        instructor2Equipment: {label: 'Equipamento', sortable: true},
        cameraman: { label: 'Camera', sortable: true },
        cameramanEquipment: {label: 'Equipamento', sortable: true},
        actions: { label: ' ', sortable: false}
      },
      lastTableRowId: 0,
      tableRows: [],
      containerSuggestions: [
        " ",
        "Vector",
        "Javelin",
        "Voodoo",
        "Talon",
        "Infinity",
        "Mirage",
        "Icon",
        "Wings",
        "Vortex",
        "Quasar",
        "Dolphin"
      ],
      
      personToInclude: {},
      jumpCategoryToInclude: "",
      personContainerToInclude: "",
      personMainCanopySizeToInclude: "",
      locatorPersonEquipmentToInclude: {},
      instructor1ToInclude: {},
      instructor1ContainerToInclude: "",
      instructor1MainCanopySizeToInclude: "",
      locatorInstructor1EquipmentToInclude: {},
      instructor2ToInclude: {},
      instructor2ContainerToInclude: "",
      instructor2MainCanopySizeToInclude: "",
      locatorInstructor2EquipmentToInclude: {},
      cameramanToInclude: {},
      cameramanContainerToInclude: "",
      cameramanMainCanopySizeToInclude: "",
      locatorCameramanEquipmentToInclude: {},

      manifestInsertFailed: false,
      manifestEditFailed : false,
      insertMode : true
    }
  },

  mounted: function() {
    var self = this

    eventBus.$on("manifestInserted", function() {
      self.onManifestInserted()
    })

    eventBus.$on("manifestEdited", function() {
      self.onManifestEdited()
    })

    eventBus.$on("manifestEditFailed", function() {
      self.onManifestEditFailed()
    })

    eventBus.$on("insertManifestStart", function() {
      self.insertMode = true
      self.clearAllData()
    })

    eventBus.$on("editManifestStart", function(manifest) {
      self.insertMode = false
      self.clearAllData()
      self.manifest = manifest
      self.loadUI()
      self.manifest.items = []
    })
    
  },

  watch: {
    currentEvent: function() {
      this.manifest.eventId = this.currentEvent.id
    }
  },

  computed: {

    affLevel: function() {
      var result = ""
      if (this.jumpCategoryToInclude.key == "AFF") {
        if ((this.personToInclude.cpf) && (this.personToInclude.studentAffLevel)) {
          result = affLevelLabel + " " + this.personToInclude.studentAffLevel
        }
      }
      return result
    },

    totalPassengers: function() {
      var amount = 0
      this.manifestItemsDenormalized.map(function(item) {
        if (item.person.cpf) {
          amount++;
        }
        if (item.instructor1 && item.instructor1.cpf) {
          amount++;
        }
        if (item.instructor2 && item.instructor2.cpf) {
          amount++;
        }
        if (item.cameraman && item.cameraman.cpf) {
          amount++
        }
      })
      return amount
    },

    filteredJumpCategoryOptions: function() {
      var options = [""]
      var self = this
      switch (this.personToInclude.personCategory) {
        case "Aluno":
          options = options.concat(this.jumpCategoryOptions.filter(function(item) {
              return item.availableTo.indexOf("Student") > -1
            }))
          break
        case "Passageiro Tandem":
          options = options.concat(this.jumpCategoryOptions.filter(function(item) {
              return item.availableTo.indexOf("Tandem") > -1
            }))
          break
        default:
          options = options.concat(this.jumpCategoryOptions)
      }
      return options
    },
    amountInstructorsNeeded: function() {
      var amount = 0
      switch (this.jumpCategoryToInclude.key) {
        case "AFF":
          if (this.personToInclude.studentAffLevel < 4) {
            amount = 2
          } else if (this.personToInclude.studentAffLevel < 8) {
            amount = 1
          }
          break
        case "ASL": 
          amount = 1
          break
        case "BBF":
          amount = 1
          break
        case "TANDEM":
          amount = 1
          break
        default:
          amount = 2
          break
      }
      return amount
    },
    calculatedValue: function() {
      var amount = 0
      switch (this.jumpCategoryToInclude.key) {
        case 'AFF':
        case 'ASL':
        case 'BBF':
        case 'BELLY_FLY':
        case 'FUN':
        case 'FREE_FLY':
        case 'TR':
        case 'TRV':
          amount += 150
          if (this.instructor1ToInclude.cpf) {
            amount += 150
          }
          if (this.instructor2ToInclude.cpf) {
            amount += 150
          }
          if (this.cameramanToInclude.cpf) {
            amount += 150
          }
          if (this.locatorPersonEquipmentToInclude.cpf) {
            amount += 50
          }
          break
        case 'TANDEM':
          amount += 680
          if (this.cameramanToInclude.cpf) {
            amount += 150
          }
      }
      return amount
    },

    aircraftOptionsWithEmptyObj: function() {
      return [{}].concat(this.aircraftOptions)
    },

    peopleOptionsWithEmptyObj: function() {
      return [{}].concat(this.peopleOptions)
    }

  },

  methods: {

    onManifestInserted : function() {
      this.closeForm()
    },
    
    onManifestEdited : function() {
      this.closeForm()
    },

    closeForm : function() {
      this.insertMode = true
      this.clearAllData()
      $("#manifestFormClose").click()
    },

    saveManifest: function() {
      var self = this
      self.manifest.items = []
      this.manifestItemsDenormalized.map(function(denormalizedItem) {
        self.manifest.items.push(self.createManifestItem(denormalizedItem))
      })

      if (this.insertMode) {
        eventBus.$emit("includeManifest", $.extend({}, this.manifest))
      } else {
        eventBus.$emit("editManifestSubmit", $.extend({}, this.manifest))
      }
    },

    createManifestItem: function(denormalizedItem) {
      var personRole = {
        key: "SKYDIVER",
        value: personRoleSkydiverLabel
      }
      switch (denormalizedItem.jumpCategory.key) {
        case "TANDEM":
          personRole = {
            key: "TANDEM_PASSENGER",
            value: personRoleTandemPassengerLabel
          }
          break
      }

      var equipment = {
        container: denormalizedItem.personContainer,
        mainCanopySize: denormalizedItem.personMainCanopySize,
        owner: {
          cpf: denormalizedItem.locatorPersonEquipment ? denormalizedItem.locatorPersonEquipment.cpf : null
        }
      }
      var manifestPerson = {
        person: {
          cpf: denormalizedItem.person.cpf,
          name: denormalizedItem.person.name,
          personCategory: denormalizedItem.person.personCategory
        },
        equipment: equipment,
        role: personRole
      }
      var assistants = [
        {
          person: {
            cpf: denormalizedItem.instructor1 ? denormalizedItem.instructor1.cpf : null,
            name: denormalizedItem.instructor1 ? denormalizedItem.instructor1.name : null,
            personCategory: denormalizedItem.instructor1 ? denormalizedItem.instructor1.personCategory : null
          },
          equipment: {
            container: denormalizedItem.instructor1Container,
            mainCanopySize: denormalizedItem.instructor1MainCanopySize,
            owner: {
              cpf: denormalizedItem.locatorInstructor1Equipment ? denormalizedItem.locatorInstructor1Equipment.cpf : null,
              name: denormalizedItem.locatorInstructor1Equipment ? denormalizedItem.locatorInstructor1Equipment.name : null,
              personCategory: denormalizedItem.locatorInstructor1Equipment ? denormalizedItem.locatorInstructor1Equipment.personCategory : null
            }
          },
          role: personRole.key == "TANDEM_PASSENGER" ? 
                  {key: "TANDEM_PILOT", value: personRoleTandemPilotLabel} :
                  {key: "INSTRUCTOR", value: personRoleInstructorLabel}
        },
        {
          person: {
            cpf: denormalizedItem.instructor2 ? denormalizedItem.instructor2.cpf : null
          },
          equipment: {
            container: denormalizedItem.instructor2Container,
            mainCanopySize: denormalizedItem.instructor2MainCanopySize,
            owner: {
              cpf: denormalizedItem.locatorInstructor2Equipment ? denormalizedItem.locatorInstructor2Equipment.cpf : null
            }
          },
          role: {key: "INSTRUCTOR", value: personRoleInstructorLabel}
        },
        {
          person: {
            cpf: denormalizedItem.cameraman ? denormalizedItem.cameraman.cpf : null
          },
          equipment: {
            container: denormalizedItem.cameramanContainer,
            mainCanopySize: denormalizedItem.cameramanMainCanopySize,
            owner: {
              cpf: denormalizedItem.locatorCameramanEquipment ? denormalizedItem.locatorCameramanEquipment.cpf : null
            }
          },
          role: {key: "CAMERAMAN", value: personRoleCameramanLabel}
        },
      ]

      var manifestItem = {
        manifestPerson: manifestPerson,
        jumpCategory: denormalizedItem.jumpCategory,
        assistants: assistants
      }

      return manifestItem
    },

    clearPassengerDataToInclude: function() {
      this.personToInclude = {}
      this.jumpCategoryToInclude = "",
      this.personContainerToInclude = "",
      this.personMainCanopySizeToInclude = "",
      this.locatorPersonEquipmentToInclude = {},
      this.instructor1ToInclude = {},
      this.instructor1ContainerToInclude = "",
      this.instructor1MainCanopySizeToInclude = "",
      this.locatorInstructor1EquipmentToInclude = {},
      this.instructor2ToInclude = {},
      this.instructor2ContainerToInclude = "",
      this.instructor2MainCanopySizeToInclude = "",
      this.locatorInstructor2EquipmentToInclude = {},
      this.cameramanToInclude = {},
      this.cameramanContainerToInclude = "",
      this.cameramanMainCanopySizeToInclude = "",
      this.locatorCameramanEquipmentToInclude = {}
    },

    clearAllData: function() {
      this.manifest = {
          aircraft: {},
          eventId: this.currentEvent.id,
          altitude: "",
          items: []
      }
      this.manifestItemsDenormalized = []
      this.lastTableRowId = 0
      this.tableRows = []
      this.clearPassengerDataToInclude()
    },

    getPassengerWithId: function(person) {
      var result = person.name
      if (person.cbpqRegistration) {
        result += " (" + person.cbpqRegistration + ")"
      }
      return result
    },

    addPassenger: function() {
      this.lastTableRowId++
      var denormalizedManifestItem = {
        itemId: this.lastTableRowId,
        person: this.personToInclude,
        jumpCategory: this.jumpCategoryToInclude,
        personContainer: this.personContainerToInclude,
        personMainCanopySize: this.personMainCanopySizeToInclude,
        locatorPersonEquipment: this.locatorPersonEquipmentToInclude,
        instructor1: this.instructor1ToInclude,
        instructor1Container: this.instructor1ContainerToInclude,
        instructor1MainCanopySize: this.instructor1MainCanopySizeToInclude,
        locatorInstructor1Equipment: this.locatorInstructor1EquipmentToInclude,
        instructor2: this.instructor2ToInclude,
        instructor2Container: this.instructor2ContainerToInclude,
        instructor2MainCanopySize: this.instructor2MainCanopySizeToInclude,
        locatorInstructor2Equipment: this.locatorInstructor2EquipmentToInclude,
        cameraman: this.cameramanToInclude,
        cameramanContainer: this.cameramanContainerToInclude,
        cameramanMainCanopySize: this.cameramanMainCanopySizeToInclude,
        locatorCameramanEquipment: this.locatorCameramanEquipmentToInclude
      }
      this.manifestItemsDenormalized.push(denormalizedManifestItem)
      this.tableRows.push(this.createTableRow(denormalizedManifestItem))
      this.clearPassengerDataToInclude()
    },

    createTableRow: function(denormalizedManifestItem) {
      var tableRow = { 
        tableRowId: denormalizedManifestItem.itemId,
        person: this.formatPassengerName(denormalizedManifestItem.person.name),
        jumpCategory: denormalizedManifestItem.jumpCategory.value,
        personEquipment: this.getMergedEquipmentData(denormalizedManifestItem.personContainer, denormalizedManifestItem.personMainCanopySize, denormalizedManifestItem.locatorPersonEquipment),
        instructor1: this.formatPassengerName(denormalizedManifestItem.instructor1 ? denormalizedManifestItem.instructor1.name : null),
        instructor1Equipment: this.getMergedEquipmentData(denormalizedManifestItem.instructor1Container, denormalizedManifestItem.instructor1MainCanopySize, denormalizedManifestItem.locatorInstructor1Equipment),
        instructor2: this.formatPassengerName(denormalizedManifestItem.instructor2 ? denormalizedManifestItem.instructor2.name : null),
        instructor2Equipment: this.getMergedEquipmentData(denormalizedManifestItem.instructor2Container, denormalizedManifestItem.instructor2MainCanopySize, denormalizedManifestItem.locatorInstructor2Equipment),
        cameraman: this.formatPassengerName(denormalizedManifestItem.cameraman ? denormalizedManifestItem.cameraman.name : null),
        cameramanEquipment: this.getMergedEquipmentData(denormalizedManifestItem.cameramanContainer, denormalizedManifestItem.cameramanMainCanopySize, denormalizedManifestItem.locatorCameramanEquipment)
      }
      return tableRow
    },

    removeManifestItem: function(rowLine) {
      this.manifestItemsDenormalized = this.manifestItemsDenormalized.filter(function(item) {
        return item.itemId != rowLine.tableRowId
      })

      this.tableRows = this.tableRows.filter(function(item) {
        return item.tableRowId != rowLine.tableRowId
      })
    },

    getMergedEquipmentData: function(container, mainCanopySize, locator) {
      var result = container || ""
      if (mainCanopySize) {
        result += " " + mainCanopySize
      }
      if ((locator) && (locator.name)) {
        result +=  "(" + this.formatPassengerName(locator.name) + ")"
      }
      return result
    },

    formatPassengerName: function(name) {
      var resultName = ""
      if (name) {
        resultName = name.slice(0, name.indexOf(' '))
        var lastName = name.split(' ').slice(-1).join(' ')
        if (lastName.trim()) {
          resultName += ' ' + lastName.charAt(0)
        }
      }
      return resultName
    },

    loadUI: function() {
      var self = this
      this.manifest.items.map(function(manifestItem){
        var tableRow = self.denormalizeManifestItem(manifestItem)
        self.lastTableRowId++
        tableRow.itemId = self.lastTableRowId
        self.manifestItemsDenormalized.push(tableRow)
        self.tableRows.push(self.createTableRow(tableRow))
      })
      this.clearPassengerDataToInclude()
    },

    denormalizeManifestItem: function(manifestItem) {
      var instructors = []
      var cameraman = {}
      manifestItem.assistants.map(function(assistant) {
        switch (assistant.role.key) {
          case "CAMERAMAN":
            cameraman = assistant
            break
          default:
            instructors.push(assistant)
        }
      })

      while (instructors.length < 2) {
        instructors.push({})
      }

      var tableItem = {
        person: manifestItem.manifestPerson.person,
        jumpCategory: manifestItem.jumpCategory,
        personContainer: manifestItem.equipment ? manifestItem.equipment.container : null,
        personMainCanopySize: manifestItem.equipment ? manifestItem.equipment.mainCanopySize : null,
        locatorPersonEquipment: manifestItem.equipment ? manifestItem.equipment.owner : null,
        instructor1: instructors[0].person,
        instructor1Container: instructors[0].equipment ? instructors[0].equipment.container : null,
        instructor1MainCanopySize: instructors[0].equipment ? instructors[0].equipment.mainCanopySize : null,
        locatorInstructor1Equipment: instructors[0].equipment ? instructors[0].equipment.owner : null,
        instructor2: instructors[1].person,
        instructor2Container: instructors[1].equipment ? instructors[0].equipment.container : null,
        instructor2MainCanopySize: instructors[1].equipment ? instructors[0].equipment.mainCanopySize : null,
        locatorInstructor2Equipment: instructors[1].equipment ? instructors[0].equipment.owner : null,
        cameraman: cameraman.person,
        cameramanContainer: cameraman.equipment ? cameraman.equipment.container : null,
        cameramanMainCanopySize: cameraman.equipment? cameraman.equipment.mainCanopySize : null,
        locatorCameramanEquipment: cameraman.equipment ? cameraman.equipment.owner : null
      }

      return tableItem
    },

    onManifestEditFailed : function() {
      var self = this
      this.manifestEditFailed = true
      setTimeout(function() {
        self.manifestEditFailed = false
      }, 2000)
    },

  }
}
