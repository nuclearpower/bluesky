var vmManifestList = {

  template : "#manifest-list-template",

  props : [
    "manifests", 
    "currentEvent",
    "aircraftOptions",
    "peopleOptions",
    "jumpCategoryOptions"
  ],

  data: function(){
    return {
      searchToday: true
    }
  },

  components : {
    "manifest" : vmManifest,
    "manifest-form": vmManifestForm
  },

  mounted: function() {
    var self = this
    eventBus.$on("updatedManifestList", function(manifests) {
      self.manifests = manifests
    })
    eventBus.$on("manifestEdited", function() {
      self.refreshSearch()
    })
  },

  watch: {
    searchToday: function() {
      this.refreshSearch()
    }
  },

  methods: {
    setTodaySearch: function(isToday) {
      this.searchToday = isToday
    },

    insertManifest : function() {
      eventBus.$emit("insertManifestStart")
    },

    refreshSearch: function() {
      eventBus.$emit("changedManifestPeriod", this.searchToday)
    }
    
  }
}
