var eventStateMixin = {

  data : function() {
    return {
      OPEN : {
        enumType : "com.bluesky.EventStateEnum",
        name : "OPEN"
      },
      IN_PROGRESS : {
        enumType : "com.bluesky.EventStateEnum",
        name : "IN_PROGRESS"
      },
      CLOSED : {
        enumType : "com.bluesky.EventStateEnum",
        name : "CLOSED"
      }
    }
  },

  methods : {

    eventStateLabel : function(eventState) {
      var result = ""
      switch(eventState.name) {
        case "OPEN" :
          result = eventStateOpenLabel
          break
        case "IN_PROGRESS" :
          result = eventStateInProgressLabel
          break
        case "CLOSED" : 
          result = eventStateClosedLabel
          break
      }
      return result
    },

    getState : function(eventStateName) {
      var result = ""
      switch(eventStateName) {
        case "OPEN" :
          result = this.OPEN
          break
        case "IN_PROGRESS" :
          result = this.IN_PROGRESS
          break
        case "CLOSED" : 
          result = this.CLOSED
          break
      }
      return result
    },

    isState : function(state1, state2) {
      return state1.name == state2.name
    },

    isOpen : function(state) {
      return this.isState(state, this.OPEN)
    },

    isInProgress : function(state) {
      return this.isState(state, this.IN_PROGRESS)
    }
  }
}
