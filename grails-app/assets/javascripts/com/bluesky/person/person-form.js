var vmPersonForm = {
  template : "#person-form-template",
  
  props : ["person"],

  components : {
    "spinner": vmSpinner
  },
  
  methods: {
    savePerson: function() {
      var self = this
      var postUrl = this.insertMode ? insertPersonURL : updatePersonURL
      $.post(postUrl, {person: JSON.stringify(self.person)})
      .done(function(data) {
        switch(data) {
          case 'Updated':           
            eventBus.$emit('personUpdated', self.person)
            break
          case 'Inserted':
            eventBus.$emit('personInserted', self.person)
            break
          case 'InsertFailed':
            self.onPersonInsertFailed()
            break
          case 'InsertDuplicated':
            self.onPersonInsertDuplicated()
            break
          default: break;
        }
      })
      .fail(function() {
        self.onPersonOperationError()
        console.log("ERROR")
      })
    },

    onPersonInsertDuplicated : function() {
      var self = this
      this.personInsertDuplicated = true
      setTimeout(function() {
        self.personInsertDuplicated = false
      }, 2000)
    },

    onPersonInsertFailed : function() {
      var self = this
      this.personInsertFailed = true
      setTimeout(function() {
        self.personInsertFailed = false
      }, 2000)
    },

    onPersonOperationError : function() {
      var self = this
      this.personOperationFailed = true
      setTimeout(function() {
        self.personOperationFailed = false
      }, 2000)
    }
  },

  data: function() {
    return {
      insertMode: false,
      personInsertDuplicated : false,
      personInsertFailed : false,
      personUpdateFailed : false,
      personOperationFailed : false
    }
  },

  computed: {
    isStudent: function() {
      return this.person.personCategory == "Aluno"
    },

    isAffStudent: function() {
      return this.isStudent && this.person.studentCourseType == "AFF"
    },

    isAthlete: function() {
      return this.person.personCategory == "Atleta"
    }

  },

  mounted: function() {
    var self = this
    eventBus.$on('personUpdated', function() {
      $("#closeUpdatePerson").click();
    })
    eventBus.$on('personInserted', function() {
      $("#closeUpdatePerson").click();
    })
    eventBus.$on('personInsertModeChanged', function(insertMode) {
      self.insertMode = insertMode
    })
  }
}
