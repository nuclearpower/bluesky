var vmPersonTable = {

  template : "#person-table-template",

  components : {
    "person-form": vmPersonForm
  },

  props : ["persons"],

  data : function() {
    return {
      selectedPerson: "",
      sortBy: null,
      sortDesc: false,
      filter: null,
      currentPage: 1,
      perPage: 5,
      totalRows: 0,
      fields: [
        { key: 'personCategory', label: ' ', sortable: false },
        { key: 'name', label: personNameLabel, sortable: true },
        { key: 'cpf', label: personCpfLabel, sortable: true},
        { key: 'cbpqRegistration', label: personCbpqRegistrationLabel, sortable: true},
        { key: 'phoneNumber', label: personPhoneLabel, sortable: false},
        { key: 'emergencyPhoneNumber', label: personEmergencyPhoneLabel, sortable: false},
        { key: 'weight', label: personWeightLabel, sortable: true},
        { key: 'actions', label: ' ', sortable: false}
      ],
      typeToSearchLabel : ""
    }
  },

  mounted : function() {
    var self = this;
    eventBus.$on('personInserted', function(person) {
      self.persons.push(person)
    })

    this.typeToSearchLabel = typeToSearchLabel
  },

  watch: {
    persons: function() {
      this.totalRows = this.persons.length;
    }
  },
  computed: {
    sortOptions () {
      // Create an options list from our fields
      return this.fields
        .filter(f => f.sortable)
        .map(f => { return { text: f.label, value: f.key } })
    }
  },

  methods: {
    selectPerson: function(person) {
      this.selectedPerson = $.extend({},person);
      if (!this.selectedPerson.cpf) {
        eventBus.$emit('personInsertModeChanged', true);
      } else {
        eventBus.$emit('personInsertModeChanged', false);
      }
    },
    removePerson: function(person) {
      if (confirm(confirmDeleteLabel)) {
        eventBus.$emit('removePerson', person);
      }
    },
    personCategoryClass: function(personCategory) {
      var cssClass = ""
      switch (personCategory) {
        case "Atleta":
          cssClass = "fa fa-child fa-fw"
          break
        case "Aluno":
          cssClass = "fa fa-graduation-cap fa-fw"
          break
        case "Passageiro Tandem":
          cssClass = "fa fa-sign-language fa-fw"
          break
        default: {
          cssClass = "fa fa-question fa-fw"
          break
        }
      }
      return cssClass
    },
    onFiltered (filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.totalRows = filteredItems.length
      this.currentPage = 1
    }
  }

  
}

