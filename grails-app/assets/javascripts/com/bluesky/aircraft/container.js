var vmAircraftContainer = {

  template : "#aircraft-container-template",

  props : [
    "aircrafts"
  ],

  components : {
    "aircraft-form" : vmAircraftForm
  },

  data : function() {
    return {
      filter : "",
      totalRows: 0,
      currentPage : 1,
      fields: [
        { key: 'registration', label: aircraftRegistrationLabel, sortable: true },
        { key: 'model', label: aircraftModelLabel, sortable: true },
        { key: 'capacity', label: aircraftCapacityLabel, sortable: true},
        { key: 'actions', label: ' ', sortable: false}
      ],
      perPage: 5,
      sortBy: null,
      sortDesc: false,
      typeToSearchLabel : ""
    }
  },

  mounted : function() {

    this.typeToSearchLabel = typeToSearchLabel
  },

  methods : {
    insertAircraft : function() {
      eventBus.$emit("insertAircraftStart")
    },
    
    editAircraft : function(aircraft) {
      eventBus.$emit("editAircraftStart", $.extend({}, aircraft))
    },

    removeAircraft : function(aircraft) {
      if (confirm(confirmDeleteLabel)) {
        eventBus.$emit("removeAircraft", aircraft)
      }
    },

    onFiltered (filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.totalRows = filteredItems.length
      this.currentPage = 1
    }
  }
}

