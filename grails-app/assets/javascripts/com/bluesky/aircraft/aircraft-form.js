var vmAircraftForm = {
  template: "#aircraft-form-template",

  data : function() {
    return {
      aircraft : {
        registration : "",
        model : "",
        capacity : 0
      },
      aircraftInsertDuplicated : false,
      aircraftInsertFailed : false,
      aircraftEditFailed : false,
      insertMode : true
    }
  },

  mounted : function() {
    var self = this

    eventBus.$on("insertAircraftStart", function() {
      self.insertMode = true
      self.clear()
    })

    eventBus.$on("aircraftInsertDuplicated", function() {
      self.onAircraftInsertDuplicated()
    })

    eventBus.$on("aircraftInserted", function() {
      self.onAircraftInserted()
    })

    eventBus.$on("aircraftInsertFailed", function() {
      self.onAircraftInsertFailed()
    })

    eventBus.$on("editAircraftStart", function(aircraft) {
      self.insertMode = false
      self.aircraft.registration = aircraft.registration
      self.aircraft.model = aircraft.model
      self.aircraft.capacity = aircraft.capacity
    })

    eventBus.$on("aircraftEditFailed", function() {
      self.onAircraftEditFailed()
    })

    eventBus.$on("aircraftEdited", function() {
      self.onAircraftEdited()
    })
  },

  watch : {
    "aircraft.registration" : function() {
      this.aircraft.registration = this.aircraft.registration.toUpperCase()
    }
  },

  computed : {
    editMode : function() {
      return !this.insertMode
    }
  },

  methods : {

    saveAircraft : function() {
      if(this.insertMode) {
        eventBus.$emit("includeAircraft", $.extend({}, this.aircraft))
      } else {
        eventBus.$emit("editAircraftSubmit", this.aircraft)
      }
    },

    onAircraftInserted : function() {
      this.closeForm()
    },

    onAircraftInsertDuplicated : function() {
      var self = this
      this.aircraftInsertDuplicated = true
      setTimeout(function() {
        self.aircraftInsertDuplicated = false
      }, 2000)
    },

    onAircraftInsertFailed : function() {
      var self = this
      this.aircraftInsertFailed = true
      setTimeout(function() {
        self.aircraftInsertFailed = false
      }, 2000)
    },

    onAircraftEditFailed : function() {
      var self = this
      this.aircraftEditFailed = true
      setTimeout(function() {
        self.aircraftEditFailed = false
      }, 2000)
    },

    onAircraftEdited : function() {
      this.closeForm()
    },

    clear : function() {

      if(this.insertMode) {
        this.aircraft.registration = ""
      }
      this.aircraft.model = ""
      this.aircraft.capacity = 0;
    },

    closeForm : function() {
      this.insertMode = true
      this.clear()
      $("#aircraftFormClose").click()
    }
  }
}
