var vmLocationForm = {
  template: "#location-form-template",

  data : function() {
    return {
      location : {
        id: -1,
        name : "",
      },
      locationInsertDuplicated : false,
      locationInsertFailed : false,
      locationEditFailed : false,
      insertMode : true
    }
  },

  mounted : function() {
    var self = this

    eventBus.$on("insertLocationStart", function() {
      self.insertMode = true
      self.clear()
    })

    eventBus.$on("locationInsertDuplicated", function() {
      self.onLocationInsertDuplicated()
    })

    eventBus.$on("locationInserted", function() {
      self.onLocationInserted()
    })

    eventBus.$on("locationInsertFailed", function() {
      self.onLocationInsertFailed()
    })

    eventBus.$on("editLocationStart", function(location) {
      self.insertMode = false
      self.location.id = location.id
      self.location.name = location.name
    })

    eventBus.$on("locationEditFailed", function() {
      self.onLocationEditFailed()
    })

    eventBus.$on("locationEdited", function() {
      self.onLocationEdited()
    })
  },

  methods : {

    saveLocation : function() {
      if(this.insertMode) {
        eventBus.$emit("includeLocation", $.extend({}, this.location))
      } else {
        eventBus.$emit("editLocationSubmit", this.location)
      }
    },

    onLocationInserted : function() {
      this.closeForm()
    },

    onLocationInsertDuplicated : function() {
      var self = this
      this.locationInsertDuplicated = true
      setTimeout(function() {
        self.locationInsertDuplicated = false
      }, 2000)
    },

    onLocationInsertFailed : function() {
      var self = this
      this.locationInsertFailed = true
      setTimeout(function() {
        self.locationInsertFailed = false
      }, 2000)
    },

    onLocationEditFailed : function() {
      var self = this
      this.locationEditFailed = true
      setTimeout(function() {
        self.locationEditFailed = false
      }, 2000)
    },

    onLocationEdited : function() {
      this.closeForm()
    },

    clear : function() {
      this.location.id = -1
      this.location.name = ""
    },

    closeForm : function() {
      this.insertMode = true
      this.clear()
      $("#locationFormClose").click()
    }
  }
}
