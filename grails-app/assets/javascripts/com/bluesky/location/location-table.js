var vmLocationTable = {
  template: "#location-table-template",

  props : ["locations"],

  components : {
    "location-form" : vmLocationForm
  },

  data : function() {
    return {
      filter : "",
      totalRows: 0,
      currentPage : 1,
      fields: [
        { key: 'name', label: locationNameLabel, sortable: true },
        { key: 'actions', label: ' ', sortable: false}
      ],
      perPage: 5,
      sortBy: null,
      sortDesc: false,
      typeToSearchLabel : ""
    }
  },

  mounted : function() {

    this.typeToSearchLabel = typeToSearchLabel
  },

  methods : {
    insertLocation : function() {
      eventBus.$emit("insertLocationStart")
    },
    
    editLocation : function(location) {
      eventBus.$emit("editLocationStart", $.extend({}, location))
    },

    removeLocation : function(location) {
      if (confirm(confirmDeleteLabel)) {
        eventBus.$emit("removeLocation", location)
      }
    },

    onFiltered (filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.totalRows = filteredItems.length
      this.currentPage = 1
    }
  }
}
