var vmDefaultValuesForm = {
  template: "#default-values-form-template",

  props : ["defaultValues"],

  data : function() {
    return {
      defaultValuesEditFailed : false,
      defaultValuesEditSucceed: false
    }
  },

  mounted: function() {
    var self = this
    eventBus.$on('defaultValuesUpdated', function(defaultValues) {
      self.onDefaultValuesEditedSucceed()
    })
  },

  methods : {

    saveDefaultValues : function() {
      var self = this
      $.post(updateDefaultValuesURL, {defaultValues: JSON.stringify(self.defaultValues)})
      .done(function(data) {
        switch(data) {
          case 'Updated':           
            eventBus.$emit('defaultValuesUpdated', self.defaultValues)            
            break
          case 'UpdateFailed':
            self.onDefaultValuesEditFailed()
            break
          default: break;
        }
      })
      .fail(function() {
        console.log("ERROR")
      })
    },

    onDefaultValuesEditedSucceed : function() {
      var self = this
      this.defaultValuesEditSucceed = true
      setTimeout(function() {
        self.defaultValuesEditSucceed = false
      }, 2000)
    },

    onDefaultValuesEditFailed : function() {
      var self = this
      this.defaultValuesEditFailed = true
      setTimeout(function() {
        self.defaultValuesEditFailed = false
      }, 2000)
    },

    clear : function() {
      this.defaultValues.clientId = 1;
      this.defaultValues.takeOff = 0;
      this.defaultValues.tandemSkydive = 0;
      this.defaultValues.parachutePacking = 0;
      this.defaultValues.equipmentRental = 0;
    },

  }
}
