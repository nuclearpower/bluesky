var vmEventTable = {

  template : "#event-table-template",

  props : [
    "events"
  ],

  components : {
    // "event-form" : vmEventForm
  },

  data : function() {
    return {
      filter : "",
      totalRows: 0,
      currentPage : 1,
      fields: [
        { key: 'name', label: eventNameLabel, sortable: true },
        { key: 'startDate', label: eventStartDateLabel, sortable: true },
        { key: 'finishDate', label: eventFinishDateLabel, sortable: true},
        { key: 'state', label: eventStateLabel, sortable: true},
        { key: 'actions', label: ' ', sortable: false}
      ],
      perPage: 5,
      sortBy: null,
      sortDesc: false,
      typeToSearchLabel : ""
    }
  },

  mounted : function() {

    this.typeToSearchLabel = typeToSearchLabel
  },

  methods : {
    insertEvent : function() {
      eventBus.$emit("insertEventStart")
    },
    
    editEvent : function(event) {
      eventBus.$emit("editEventStart", $.extend({}, event))
    },

    removeEvent : function(event) {
      if (confirm(confirmDeleteLabel)) {
        eventBus.$emit("removeEvent", event)
      }
    },

    onFiltered (filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.totalRows = filteredItems.length
      this.currentPage = 1
    },

    setCurrent : function(event) {
      eventBus.$emit("setCurrentEvent", event)
    }
  }
}

