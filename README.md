Escopo do Projeto 
=======

Uma área de salto é formada pelos seguintes elementos:  
* Avião:  
	* Possui um prefixo que o identifica, ex.: PT-BMZ  
	* Possui um modelo, ex.: Cessna 180.  
* Paraquedistas:  
    * Possuem todos os dados de uma pessoa física, mas os principais são:  
		* Nome  
		* Telefone  
		* Número do cadastro CBPq  
		* Categoria (A, B, C ou D)  
		* Telefone para emergência  
		* Grupo sanguíneo e fator RH  
		* Peso  
		* Alergias e observações médicas  
* Passageiros Tandem:  
	* São as pessoas que realizam saltos duplos, vão presas a um instrutor (paraquedista).  
* Equipamentos:  
	* Nome identificador: Geralmente se utiliza a marca do container (ex.: Vector) e tamanho do velame (ex.: 170)  
	* Número identificador: É uma etiqueta com numeração que é geralmente utilizado em grandes eventos para diferenciar os equipamentos.  
	* Dono do equipamento: pode ser um paraquedista, uma escola, ou qualquer outra PF ou PJ.  

**Funcionamento de uma área de salto:**
Paraquedista se manifesta (informa que deseja saltar).  
Responsável pela área monta as decolagens. Uma decolagem é feita por um avião, contém um grupo de paraquedistas e cada paraquedista sobe com um equipamento (próprio ou alugado).  
Ao chegar no solo, o paraquedista dobra o seu equipamento ou paga para um dobrador dobrar.  
Ao final da área, cada dobrador cobra dos paraquedistas o que lhe devem. O responsável pela área cobra dos paraquedistas o valor da decolagem e o aluguel de equipamento caso tenha sido feito.  
O pagamento dos saltos duplos é realizado pelo passageiro antes de decolar, inclusive já pode ter feito o depósito de todo o valor ou de parte.  
O pagamento pode ser realizado em dinheiro, em cartão de crédito (bandeira do cartão e número de parcelas), em cartão de débito, via depósito, cheque.

É importante que seja possível:  
* Verificar quantas decolagens ocorreram no dia ou no período de dias.
* Verificar quantos passageiros tandem saltaram no dia ou no período de dias.
* Verificar quantos paraquedistas saltaram no dia ou no período de dias.
* Verificar quantos saltos ocorreram no dia ou no período de dias.
* Verificar para um dono de equipamento quantos alugueis foram realizados e quem os realizou.
* Verificar quem já pagou seus saltos, aluguéis, dobragens e quem não pagou.
* As decolagens são montadas aos poucos. Seria interessante abrir um painel onde fosse possível ver as decolagens montadas e quem está nelas.
* Quanto foi pago em dinheiro, em débito, em crédito, etc.	
